--- /home/ivan/Downloads/pipe421original/src/pipe/models/PipeApplicationModel.java	2011-09-14 01:20:28.000000000 +0200
+++ /home/ivan/PeabraiN421/src/pipe/models/PipeApplicationModel.java	2013-02-07 20:12:12.000000000 +0100
@@ -23,6 +23,7 @@
     public FileAction exportTNAction;
     public FileAction exportPSAction;
     public FileAction importAction;
+    public FileAction settingsAction;
 
     public EditAction copyAction;
     public EditAction cutAction;
@@ -96,7 +97,9 @@
         pasteAction = new EditAction("Paste", "Paste (Ctrl-V)", "ctrl V");
         deleteAction = new DeleteAction("Delete", "Delete selection", "DELETE");
         selectAction = new TypeAction("Select", Constants.SELECT, "Select components", "S", true);
+        settingsAction = new FileAction("Settings", "Settings", "ctrl T");
 
+        
         Action actionListener = new AbstractAction()
         {
             public void actionPerformed(ActionEvent actionEvent)
@@ -214,6 +217,7 @@
     {
         saveAction.setEnabled(status);
         saveAsAction.setEnabled(status);
+        settingsAction.setEnabled(status);
 
         placeAction.setEnabled(status);
         arcAction.setEnabled(status);
