--- /home/ivan/Downloads/pipe421original/src/pipe/views/PipeApplicationView.java	2012-08-02 08:37:56.000000000 +0200
+++ /home/ivan/PeabraiN421/src/pipe/views/PipeApplicationView.java	2014-01-05 10:46:25.100519561 +0100
@@ -258,6 +258,9 @@
             e.printStackTrace();
         }
 
+        addMenuItem(fileMenu, _applicationModel.settingsAction);
+        exportMenu.setIcon(new ImageIcon(Thread.currentThread().getContextClassLoader().getResource(ApplicationSettings.getImagePath() + "Export.png")));
+        fileMenu.addSeparator();
         addMenuItem(fileMenu, _applicationModel.exitAction);
 
         JMenu editMenu = new JMenu("Edit");
@@ -677,21 +680,20 @@
 
     public void createNewTab(File file, boolean isTN)
     {
-         int freeSpace = _applicationController.addEmptyPetriNetTo(_petriNetTabs);;
+    	int freeSpace = _applicationController.addEmptyPetriNetTo(_petriNetTabs);;
 
         String name = "";
         if(_applicationController.isPasteInProgress())
         {
             _applicationController.cancelPaste();
         }
-
+        
         PetriNetView petriNetView = getPetriNetView(freeSpace);
         PetriNetTab petriNetTab = getTab(freeSpace);
 
         petriNetView.addObserver(petriNetTab); // Add the view as Observer
         petriNetView.addObserver(this); // Add the app window as
         // observer
-
         if(file == null)
         {
             name = "Petri net " + (_applicationModel.newPetriNetNumber());
