REM Script for running PeabraiN

REM Set this variable properly (set to assumed installation directory by default)
set GLPKDIR=C:\glpk-4.47\w32
REM set GLPKDIR=C:\glpk-4.47\w64
REM Uncomment the above one if you are using a 64bits environment

REM Run PeabraiN with PIPE
java -cp bin/lib/*;bin/. -Djava.library.path=%GLPKDIR% Pipe
