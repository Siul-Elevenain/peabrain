/**
 * Filename: SPNSimulator.java
 * Date: January, 2012 -- first release
 * 
 * Allows to run the simulator for GSPNs from console
 * 
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */

package pipe.modules.gspnsimulator;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import pipe.modules.bounds.dataLayer.PetriNetModel;
import pipe.modules.bounds.errors.ResultException;
import pipe.views.PetriNetView;

//TODO
//- Average place marking is not considered as input parameter!

public class SPNSimulator {

	private static void usage()
	{
		System.out.println("SPNSimulator error -- insufficient parameters.");
		System.out.println("Invoke as: java SPNSimulator <PN_in_PNML_format> <confidence_level> <error_accuracy> [options]\n");
		System.out.println("OPTIONS:" 
							+ "\n\t-tf FILENAME\t\t\t"
							+ "Transitions to be observed, collected from a file (one transition per line)"
							+ "\n\t-t TRANSITION\t\t\t"
							+ "Transition to be observed. You can define it on-the-fly, no file is needed for one single transition"
							+ "\n\t-max MAX_REPLICATIONS\t\t"
							+ "Maximum no. of replications to be performed in the simulation"
							+ "\n\t-min MIN_REPLICATIONS\t\t"
							+ "Minimum no. of replications to be performed in the simulation"
							+ "\n\t-time MAX_SIMULATION_TIME\t"
							+ "Maximum simulation time (useful for transitory simulation)"
							+ "\n\t-s [approx|exact]\t\t"
							+ "Performs an approximate or an exact stochastic simulation (by default, exact method is selected)"
							+ "\n\n");
		System.out.println("If you want to run in GUI mode, just execute: 'java SPNSimulator' (no parameters needed)");
		System.out.println("Example (CLI):\n Execute 'java SPNSimulator myFile.pnml 0.95 0.05 -t t0' for simulating" + 
						"'myFile.pnml' with\n a confidence level of 0.95 and an accuracy error of 0.05, without a maximum of simulation time while observing 't0'.");

		System.exit(-1);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		/* Receives as first parameter the PNML file to simulate */
		if(args.length == 0)
		{
			System.out.println("Launching GSPN Simulator GUI...");
			/* Launch GUI format*/
			SPNSimulatorGUI sGUI = new SPNSimulatorGUI();
		
			sGUI.setVisible(true);
			
		}else if(!(args.length < 3 || args.length > 10)){
			/* Launch CLI format */
			SPNSimulator sim = new SPNSimulator();
			
			/* Ok, now invoke simulation */
			double 	maxTime = -1;
			int minReplications = 10, maxReplications = -1;			
			
			/* parse options */
			boolean transitionFileFlag = false;
			String transitionFile = null,
					idxIDTransToObserve = null,
					ssimMethod = null;
			SimulationMethod simMethod = SimulationMethod.EXACT_SIM; // XXX, by default, exact simulation method
			
			double confLevel = 0, 
					errorAcc = 0;
			try{
				confLevel = Double.parseDouble(args[1]);
				errorAcc = Double.parseDouble(args[2]);
				
				if((errorAcc > 1 || errorAcc <= 0) || (confLevel > 1 || confLevel <= 0))
					throw new Exception();
			}
			catch(Exception e)
			{
				System.err.println("Error arise while getting either confidence level or error accuracy.");
				//e.printStackTrace()
				usage();
			}
			
			for(int i = 3; i < args.length; i = i + 2)
			{
				if(args[i].equals("-tf"))
				{
					transitionFileFlag = true;
					transitionFile = args[i + 1]; 
					System.out.println("Paremeter transition file processed [" + transitionFile + "]");
				}else if(args[i].equals("-t"))
				{
					idxIDTransToObserve = args[i + 1];
					System.out.println("Paremeter observed transition [" + idxIDTransToObserve + "]");
				}
				else if(args[i].equals("-max"))
				{
					maxReplications = Integer.parseInt(args[i + 1]);
					System.out.println("Paremeter max. no. replications processed [" + maxReplications + "]");
				}
				else if(args[i].equals("-min"))
				{
					minReplications = Integer.parseInt(args[i + 1]);
					System.out.println("Paremeter min. no. replications processed [" + minReplications + "]");
				}
				else if(args[i].equals("-time"))
				{
					maxTime = Double.parseDouble(args[i + 1]);
					System.out.println("Paremeter max. simulation time processed [" + maxTime + "]");
				}
				else if(args[i].equals("-s"))
				{
					ssimMethod = args[i + 1];
					if(ssimMethod.equals("approx"))
						simMethod = SimulationMethod.APPROX_SIM;
					System.out.println("Simulation method processed [" + simMethod + "]");
				}else
				{
					System.err.println("Error: parameter " + args[i]  + " " + args[i + 1] 
										+ " not known. Please check input parameters format.");
				}
			}
			
			
			try
			{
				String[] aux;
				
				if(transitionFileFlag)
				{
					aux = sim.parseTransitionsFile(transitionFile);
				}else
				{
					aux = new String[1];
					aux[0] = idxIDTransToObserve;
				}
					
				sim.simulate(args[0], simMethod, minReplications, maxReplications, aux, maxTime, confLevel, errorAcc);
			}
			catch(Exception e)
			{
				System.err.println("ERROR while parsing " + args[0] + ".\nPlease check that input file exists.");
				e.printStackTrace();
			}
		}else
		{
			usage();
			System.exit(-1);
		}
	}

	private void simulate(String filename, SimulationMethod simMethod, int minReplications, int maxReplications, String[] idxIDTrans, 
							double maxTime, double confLevel, double accError) throws ResultException{
		/* Create PetriNetModel from PNML filename */
		PetriNetModel pnModel = new PetriNetModel(new PetriNetView(filename));
		
		System.out.println("Simulating Petri net [" + filename + "]");
		
		Simulator sim = createSimulator(simMethod, pnModel);
		
		/* Simulate */
		sim.simulate(idxIDTrans, minReplications, maxReplications, maxTime, confLevel, accError);
		/* Get results */
		System.out.println(getPlainText(sim.printResult()));
		/* Print results to file */
		String _filename = filename.substring(0, filename.lastIndexOf('.')) + ".sta"; // Results file
		FileWriter f;
		try {
			f = new FileWriter(_filename);
			sim.printResultsToFile(f);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static Simulator createSimulator(SimulationMethod simMethod, PetriNetModel pnModel)
	{
		Simulator sim = null;
		if(simMethod ==  SimulationMethod.EXACT_SIM)
			sim = new ExactSimulator(pnModel);
		else if(simMethod ==  SimulationMethod.APPROX_SIM)
			sim = new ApproximateSimulator(pnModel);
		
		return sim;
	}
	
	private String getPlainText(String printResult) {
		return printResult.replaceAll("<br/>", "\n") // Replace <br> by \n
					.replaceAll("\\<.*?\\>", "");	// Replace the rest of HTML tags by nothing...
	}

	public String[] parseTransitionsFile(String filename)
	{
		ArrayList<String> al = new ArrayList<String>(1);
		
		try {
			FileInputStream fstream = new FileInputStream(filename);
			// Get the object of DataInputStream
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			
			String strLine = br.readLine();
			while (strLine != null)
			{
				al.add(strLine);
				strLine = br.readLine(); // Read next one
			}	
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String[] aux = new String[al.size()];
		int i = 0;
		for(String s : al)
			aux[i++] = s;
		
		return aux;
	}

}
