/**
 * Filename: ApproximateSimulator.java
 * Date: August, 		2014 -- Iván Pamplona
 * 
 * Implements an approximate method simulator for GSPNs using tau-leaping method.
 *  For more information, check 
 *     "Simulation Methods in System Biology", D. T. Gillespie, in Formal Methods for Computational Systems Biology, 2008
 *     and
 *     "Stochastical Simulation of Chemical Kinetics", D. T. Gillespie, in Annu. Rev. Phys. Chem. 2007, 58:35--55
 * 
 * @author (C) Iván Pamplona (University of Zaragoza, 2014) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */

package pipe.modules.gspnsimulator;

import java.util.ArrayList;
import java.util.Random;

import pipe.modules.bounds.dataLayer.PetriNetModel;
import umontreal.iro.lecuyer.probdist.PoissonDist;

public class ApproximateSimulator extends Simulator {

	public ApproximateSimulator(PetriNetModel pnModel) {
		super(pnModel);
	}
	
	@Override
	protected double runSimulationMethod(int[] marking, double maxSimulation,
			double[] obsTrans) {
		double simTime = 0.0;
		double tau = 0.0;

		TransitionEventList aux = null;
		ArrayList<Simulator.TimedTransitionConflict> timedInConflict 
														= new ArrayList<Simulator.TimedTransitionConflict>(1);	

		
		marking = fireImmediateTransitionsAt(marking, simTime);	
		
		ArrayList<Integer> enabledTransitions = getEnabledTransitionsAt(TransitionType.TIMED_TRANS, marking, timedInConflict);
		tau = calculateTau(marking, enabledTransitions);
		aux = generateEventList(marking, enabledTransitions, tau);
		
		if (aux.al.size() == 0) {
			System.err.println("Check initial marking. No evolving is possible.");
			return 0;
		}

		// NextEvent ne = getNextEvent(aux);
		for (int i = 0; i < aux.al.size(); i++) {
			marking = firesTransitionAt(aux.al.get(i).idxTransition, marking, (int) aux.al.get(i).minValue, simTime, timedInConflict);
		}

		do {
			simTime += tau;
			marking = fireImmediateTransitionsAt(marking, simTime);
			enabledTransitions = getEnabledTransitionsAt(TransitionType.TIMED_TRANS, marking, timedInConflict);
			tau = calculateTau(marking, enabledTransitions);
			
			timedInConflict = new ArrayList<Simulator.TimedTransitionConflict>(1);
			aux = generateEventList(marking, enabledTransitions, tau);
			// Check dead marking
			if (aux.al.size() == 0) {
				System.err
						.println("Ups... It seems dead marking was reached. No evolving is possible. Exiting!");
				return 0;
			}
			for (int i = 0; i < aux.al.size(); i++) {
				marking = firesTransitionAt(aux.al.get(i).idxTransition, marking, (int) aux.al.get(i).minValue, simTime, timedInConflict);
			}
			
		} while (simTime <= maxSimulation);

		// End of simulation...
		return simTime;
	}
	
	private TransitionEventList generateEventList(int[] marking, ArrayList<Integer> enabledTransitions, double tau) {

		Random r = new Random();
		//double tau = CalculateTau(marking,enabledTransitions);
		ArrayList<TransitionEvent> v = new ArrayList<TransitionEvent>(enabledTransitions.size());
		int idx, factor;
		double random, lambda, aux = 0;
		
		for (int i = 0; i < enabledTransitions.size(); i++) {
			idx = (int) enabledTransitions.get(i);
			factor = 1;
			// Change factor of exp, if needed (depends on the semantics!) for sum of ratios
			if (pnModel.isInfiniteSemantics(idx))
				factor = pnModel.getEnablingDegree(marking, idx);
			
			// Tau-Leaping computation
			random = r.nextDouble();
			lambda = pnModel.getTransitionRate(idx) * 
						pnModel.getEnablingDegree(marking, idx) * tau;

			// Get sum of ratios
			aux += factor * pnModel.getTransitionRate(idx);
			v.add(new TransitionEvent(idx, PoissonDist.inverseF(lambda, random), 0));
		}

		return new TransitionEventList(v, aux);
	}
	
	private double calculateTau (int[] marking, ArrayList<Integer> enabledTransitions){
		double a0 = 0; // Sum of Ratios
		double epsilon = 0.03;
		int idx, factor;
		
		// get a0 (sum of ratios)
		for (int i = 0; i < enabledTransitions.size(); i++) {
			idx = (int) enabledTransitions.get(i);
			factor = 1;
			// Change factor of exp, if needed (depends on the semantics!)
			if (pnModel.isInfiniteSemantics(idx))
				factor = pnModel.getEnablingDegree(marking, idx);

			a0 += factor * pnModel.getTransitionRate(idx);
		}
		
		int incidenceMatrix[][] = this.pnModel.getIncidenceMatrix(),
			auxIdxTrans, auxIncidence, auxIdxPlace;

		double 	tau = Double.MAX_VALUE,
				tautemp = Double.MAX_VALUE;
		
		// for all enabled transitions
		for (int i = 0; i < enabledTransitions.size(); i++) {
			ArrayList<Integer> places = this.pnModel.findInColPre(enabledTransitions.get(i));

			// for all places pre transition
			for (int ii = 0; ii < places.size(); ii++) {
				auxIdxPlace = places.get(ii);
				ArrayList<Integer> transitionPRE = this.pnModel.findInRowPost(auxIdxPlace); // todas las transiciones previas al lugar
				ArrayList<Integer> transitionPOST = this.pnModel.findInRowPre(auxIdxPlace); // todas las transiciones posterior al lugar
				double 	mu = 0, auxRate, auxEnabDegree, 
						sigma = 0;
				for (int iii = 0; iii < transitionPRE.size(); iii++) {
					auxIdxTrans = transitionPRE.get(iii);
					if (!this.pnModel.isImmediate(auxIdxTrans))
					{
						auxRate = this.pnModel.getTransitionRate(auxIdxTrans);
						auxEnabDegree = this.pnModel.getEnablingDegree(marking, auxIdxTrans);
						auxIncidence = incidenceMatrix[auxIdxPlace][auxIdxTrans];
						mu = mu + (auxRate* auxEnabDegree*auxIncidence);
						sigma += auxRate* auxEnabDegree*(auxIncidence*auxIncidence);
					}
				}
				for (int iii = 0; iii < transitionPOST.size(); iii++) {
					auxIdxTrans = transitionPOST.get(iii);
					if (!this.pnModel.isImmediate(auxIdxTrans))
					{
						auxRate = this.pnModel.getTransitionRate(auxIdxTrans);
						auxEnabDegree = this.pnModel.getEnablingDegree(marking, auxIdxTrans);
						auxIncidence = incidenceMatrix[auxIdxPlace][auxIdxTrans];
						mu = mu + (auxRate* auxEnabDegree*auxIncidence);
						sigma += auxRate* auxEnabDegree*(auxIncidence*auxIncidence);
					}
				}
				// Get minimum (see "Simulation Methods in System Biology", D. T. Gillespie, FMCSB 2008, page 17)
				double 	num1 = (Math.max(1, epsilon * a0)) / Math.abs(mu),
						num2 = (Math.pow(Math.max(1, epsilon * a0), 2)) / sigma,
						num = Math.min(num1, num1); //XXX num1 in both to be able to work *without* exception
				//FIXME: Something it's wrong here, as num2 can be *negative*. 
				// This is influenced by a negative marking, thus it's not solving such an issue raised 
				// by tau-leaping simulation method
				
				// Update tautemp
				if (num < tautemp) {
					tautemp = num;
				}
			}
			// Get minimum tau
			if (tau > tautemp)
				tau = tautemp;
		}
		
		return tau;
	}
	
	@Override
	protected SimulationMethod getMethod() {
		return SimulationMethod.APPROX_SIM;
	}
}
