/**
 * Filename: Simulator.java
 * Date: January, 	2012 -- first release
 *       July, 		2014 -- second release. Added support for average marking computation
 * 								and an approximate simulation method (credits to Iván Pamplona) 
 *       November,	2014 -- Implemented a method to solve timed transitions in conflict, 
 *       						following <URL>
 * 
 * Implements a simulator for GSPNs
 * 
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */

// XXX: No priority (first we fire immediate, then timed)
package pipe.modules.gspnsimulator;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;

import pipe.modules.bounds.dataLayer.PetriNetModel;
import pipe.modules.bounds.strategies.Strategy;
import umontreal.iro.lecuyer.probdist.StudentDist;

public abstract class Simulator extends Strategy {
	// private PetriNetModel pnModel;
	private int nTransitions, nPlaces; // more efficient than invoke each time
	private int[] firingCounts;
	// Avg. marking place
	private double[] averageMarking;
	private double[] lastTimeMarkedPlace;
	private SimulationResult simResult;

	// private int minReplications = 20,
	private int minReplications = 2, maxReplications;
	private double // sampleLength = -1,
	maxTime = -1;

	/* Observed transitions */
	private int[] obsTransitions = { 0 }; // 0 by default
	/* Observed places */
	private int[] obsPlaces = { 0 }; // 0 by default

	// Random parameters
	private long seed = System.currentTimeMillis();
	private Random random = new Random(seed);

	/* Abstract method to be implemented by child classes, contains the logic of the simulation */
	protected abstract double runSimulationMethod(int[] marking, double maxSimulation, double[] obsTrans);
	/* Abstract method to be implemented by child classes, prints a brief description of the simulation method */
	protected abstract SimulationMethod getMethod();
	
	/**
	 * Default constructor. It needs the Petri net model to work with
	 * 
	 * @param pnModel
	 *            Petri net model to work with
	 */
	public Simulator(PetriNetModel pnModel) {
		super(pnModel);
		try {
			this.pnModel.setM0(this.pnModel.getM0().clone());
		} catch (Exception e) {
			// Never happens
		}

		this.nPlaces = pnModel.getPlacesSize();
		this.nTransitions = pnModel.getTransitionsSize();
		this.firingCounts = new int[nTransitions];
	}
	
	private ObservationResultSim runSimulation(double maxSimulation)
	{
		int[] marking = pnModel.getM0().clone();
		int DELTA_FACTOR = 100;

		if (maxSimulation == -1) /* Not set */
		{
			// Max simulation = DELTA_FACTOR*sum(transitions delays) if no
			// chosen by the user
			maxSimulation = DELTA_FACTOR * this.pnModel.getSumOfDelays();
		}
		
		double[] obsTrans = new double[this.obsTransitions.length];
		double[] obsPlcs  = new double[this.obsPlaces.length];
		
		// Simulate
		double simTime = 0.0;
		simTime = runSimulationMethod(marking, maxSimulation, obsTrans);
		
		// Check if some error arises
		if(simTime == 0)
			return null;
		
		/* Get observation */
		for (int i = 0; i < obsTrans.length; i++) {
			obsTrans[i] = this.firingCounts[this.obsTransitions[i]] / simTime; // Take
																				// observation
		}
		
		for(int i = 0; i < obsPlcs.length; i++)
			obsPlcs[i] = this.averageMarking[this.obsPlaces[i]]/simTime;
			
		
		/* End of simulation... */
		return new ObservationResultSim(obsPlcs, obsTrans);
	}

	protected NextEvent getNextEvent(TransitionEventList tel) {
		// Uses Gillespie's exact SSA
		double r1 = random.nextDouble(), r2 = random.nextDouble();

		double nextTime = Math.log(1.0 / r1) / tel.sumOfRatios;

		int nextTransition = find(tel.al, r2 * tel.sumOfRatios, 0,
				tel.al.size() - 1);

		// DEBUG
		/* 
		 System.out.println(nextTime + ": value  " + r2*tel.sumOfRatios);
		 System.out.println(r1 + "; " + r2 + ";; " + tel.sumOfRatios);
		  
		 for(int i = 0; i < tel.al.size(); i++) 
			 System.out.println("(" +
				 this.pnModel.getTransitionRate(tel.al.get(i).idxTransition) + ")" +
				 	"[" + tel.al.get(i).minValue + "," + tel.al.get(i).maxValue + "]");
		*/ 
		return new NextEvent(tel.al.get(nextTransition).idxTransition, nextTime);
	}

	private int find(ArrayList<TransitionEvent> al, double value, int lowIdx,
			int highIdx) {
		/* Look for the smallest integer satisfying sum(a_j'), j' in 1..j > d */
		if (highIdx < lowIdx)
			return -1; // Never happens :) (I hope so :P)

		int middle = (lowIdx + highIdx) / 2;
		if (value > al.get(middle).maxValue)
			return find(al, value, middle + 1, highIdx);
		else if (value >= al.get(middle).minValue)
			return middle;
		else
			return find(al, value, lowIdx, middle - 1);
	}
	
	protected int[] fireImmediateTransitionsAt(int[] marking, double simTime) {
		ArrayList<Integer> v = getEnabledTransitionsAt(TransitionType.IMMED_TRANS, marking, null);
		int[] m = marking;
		do {
			/*
			 * try { int bytes = System.in.read(); }catch(Exception e){
			 * e.printStackTrace(); }
			 */
			for (int i = 0; i < v.size(); i++) {
				int idxTrans = (int) v.get(i);

				//m = firesTransitionAt(idxTrans, marking, 1);
				m = firesTransitionAt(idxTrans, marking, 
										this.pnModel.getEnablingDegree(marking, idxTrans), simTime, null);
			}

			v = getEnabledTransitionsAt(TransitionType.IMMED_TRANS, marking, null);
		} while (v.size() > 0);

		return m;
	}

	protected int[] firesTransitionAt(int idxTrans, int[] marking, 
										int edegree, double simTime, 
										ArrayList<Simulator.TimedTransitionConflict> timedInConflict) {
		if(idxTrans < 0)
		{
			// When negative, means that a timed transition in conflict has been fired...
			// Time to resolve it!
			ArrayList<TransitionEvent> conflictTransitions = new ArrayList<TransitionEvent>(
					2);
			double auxMin, auxMax = 0;
			int idxTrans2;
			TimedTransitionConflict tConflict = timedInConflict.get(idxTrans + 1);
			for (int i = 0; i < tConflict.idxConflictTransitions.size(); i++) {
				idxTrans2 = tConflict.idxConflictTransitions.get(i);
				auxMin = auxMax;
				auxMax += (this.pnModel.getTransitionRate(idxTrans2)/tConflict.superTransition.getRate());
				// Add to conflict set
				conflictTransitions.add(new TransitionEvent(idxTrans2,
						auxMin, auxMax));
			}
			
			// Resolve equal conflict
			TransitionEventList tel = new TransitionEventList(conflictTransitions,
					auxMax);
			// And add fired transitions to enabled ones
			double rand = random.nextDouble();
			int nextTransition = find(tel.al, rand
					* tel.sumOfRatios, 0, tel.al.size() - 1);
			
			// Change transition to be fired
			idxTrans = tel.al.get(nextTransition).idxTransition;
		}
		
		//System.out.println("Firing transition " + idxTrans + "[" + this.pnModel.getTransitionID(idxTrans) + "]..."); // Debug
		// Increment firing counter
		///firingCounts[idxTrans]++;
		firingCounts[idxTrans] = firingCounts[idxTrans] + edegree;

		int[] m = marking;
		// Remove tokens
		ArrayList<Integer> places = this.pnModel.findInColPre(idxTrans);
		for (int j = 0; j < places.size(); j++) {
			int idxPlace = (int) places.get(j);
			// Let's compute average marking...
			updateAverageMarking(idxPlace, simTime, m[idxPlace]);
			m[idxPlace] -= (this.pnModel.getPre()[idxPlace][idxTrans] * edegree);
		}
		// Set tokens
		places = this.pnModel.findInColPost(idxTrans);
		for (int j = 0; j < places.size(); j++) {
			int idxPlace = (int) places.get(j);
			// Let's compute average marking...
			updateAverageMarking(idxPlace, simTime, m[idxPlace]);
			m[idxPlace] += (this.pnModel.getPost()[idxPlace][idxTrans] * edegree);
		}

		return m;
	}
	
	private void updateAverageMarking(int idxPlace, double simTime, int currentMarking)
	{
		// Let's compute average marking...
		this.averageMarking[idxPlace] += (simTime - this.lastTimeMarkedPlace[idxPlace])*currentMarking;
		this.lastTimeMarkedPlace[idxPlace] = simTime;
	}
	

	private boolean isTypeOf(TransitionType type, int idxTransition) {
		boolean aux = this.pnModel.isImmediate(idxTransition);
		return (type == TransitionType.IMMED_TRANS && aux)
				|| (type == TransitionType.TIMED_TRANS && !aux);
	}

	class Transition{
		private int superIdxTransition; // Always negative
		private double rate;
		
		public Transition(int superIdx, double rate)
		{
			this.superIdxTransition = superIdx;
			this.rate = rate;
		}
		
		public double getRate()
		{
			return rate;
		}
		
		public int getIdx()
		{
			return superIdxTransition;
		}
	};
	class TimedTransitionConflict {
		Transition superTransition; // Transition created on-the-fly
		ArrayList<Integer> idxConflictTransitions; // List of indexes of timed transitions on conflict
	};
	
	/**
	 * Returns the subset of transitions that are enabled
	 * @param listEnabled List of enabled transitions (indexes)
	 * @param list2check List of indexes of transitions to check
	 * @return Subset of transitions of list2check contained in listEnabled
	 */
	private ArrayList<Integer> getSubsetEnabled(ArrayList<Integer> listEnabled, ArrayList<Integer> list2check)
	{
		ArrayList<Integer> subset = new ArrayList<Integer>(1);
		for(int i = 0; i < list2check.size(); i++)
		{
			int idxTrans = list2check.get(i);
			if(listEnabled.contains(idxTrans))
				subset.add(idxTrans);
		}
		return subset;
	}
	
	protected ArrayList<Integer> getEnabledTransitionsAt(TransitionType type,
			int[] marking, ArrayList<TimedTransitionConflict> timedConflicts) {
		ArrayList<Integer> v = new ArrayList<Integer>();

		// boolean[] vPlaces = new boolean[nPlaces];
		for (int i = 0; i < nTransitions; i++)
			if (isTypeOf(type, i)) {
				/* Check if this transition is enabled... */
				ArrayList<Integer> places = this.pnModel.findInColPre(i);
				boolean allMarked = true;
				for (int j = 0; j < places.size() && allMarked; j++) {
					int idxPlace = (int) places.get(j);
					allMarked &= (marking[idxPlace] >= this.pnModel.getPre()[idxPlace][i]);
					// System.out.println("marking[idxPlace]= " +
					// marking[idxPlace] + "AND getPRE...= " +
					// this.pnModel.getPre()[idxPlace][i]);
				}

				/* Check inhibitor arcs */
				places = this.pnModel.findInColPreInhibitor(i);
				for (int j = 0; j < places.size() && allMarked; j++) {
					int idxPlace = (int) places.get(j);
					allMarked &= (marking[idxPlace] < this.pnModel
							.getPreInhibitor()[idxPlace][i]);
				}
				/* done */
				if (allMarked)
					v.add(i); // Add transition if all places all enabled...
			}

		/* Look for conflicts */
		TransitionEventList tel;
		for (int i = 0; i < nPlaces; i++) {
			ArrayList<Integer> transitions = this.pnModel.findInRowPre(i);
			ArrayList<Integer> subsetConflict = getSubsetEnabled(v, transitions);
			if (subsetConflict.size() > 1)
			{
				if (areTypeOf(TransitionType.TIMED_TRANS, subsetConflict)){
					//System.err
					//		.println("Warning: conflict found on timed transitions, nothing done at respect.");
					TimedTransitionConflict newConflict = new TimedTransitionConflict();
					newConflict.idxConflictTransitions = subsetConflict;
					newConflict.superTransition = new Transition(
													timedConflicts.size() - 1, 
													// XXX Indexes are negative in this case!
													this.getRateOfListOfTransition(subsetConflict));
					timedConflicts.add(newConflict);
					v.removeAll(subsetConflict);
				}else {
					/*
					 * Conflict found on immediate transitions, then make
					 * something!
					 */
					ArrayList<TransitionEvent> conflictTransitions = new ArrayList<TransitionEvent>(
							2);
					double auxMin, auxMax = 0;
					int idxTrans;
					for (int j = 0; j < transitions.size(); j++) {
						idxTrans = transitions.get(j);
						// Remove from enabled
						v.remove(new Integer(idxTrans));
						auxMin = auxMax;
						auxMax += (this.pnModel.getTransitionWeight(idxTrans));
						// Add to conflict set
						conflictTransitions.add(new TransitionEvent(idxTrans,
								auxMin, auxMax));
					}

					/* Check priority of transitions... */
					boolean isPrioritary = false;
					int maxPriority = -1;// this.pnModel.getPriority(conflictTransitions.get(0).idxTransition);
					idxTrans = 0;
					for (int j = 0; j < conflictTransitions.size(); j++) {
						TransitionEvent te = conflictTransitions.get(j);
						if (maxPriority < this.pnModel
								.getPriority(te.idxTransition)) {
							idxTrans = te.idxTransition;
							isPrioritary = (maxPriority != -1);
							maxPriority = this.pnModel
									.getPriority(te.idxTransition);
						} else if (maxPriority > this.pnModel
								.getPriority(te.idxTransition))
							isPrioritary = true;
					}
					if (!isPrioritary) {
						// Resolve equal conflict
						tel = new TransitionEventList(conflictTransitions,
								auxMax);
						// And add fired transitions to enabled ones
						double rand = random.nextDouble();
						int nextTransition = find(tel.al, rand
								* tel.sumOfRatios, 0, tel.al.size() - 1);
						v.add(tel.al.get(nextTransition).idxTransition);
					} else
						v.add(idxTrans);
				}
			}
		}

		return v;
	}

	private boolean areTypeOf(TransitionType type,
			ArrayList<Integer> transitions) {
		for (int i = 0; i < transitions.size(); i++)
			if (!isTypeOf(type, transitions.get(i)))
				return false;

		return true;
	}

	protected TransitionEventList generateEventList(int[] marking,
			ArrayList<Integer> enabledTransitions,
			ArrayList<Simulator.TimedTransitionConflict> timedInConflict) {
		
		ArrayList<TransitionEvent> v = new ArrayList<TransitionEvent>(1);
		double aux = 0, aux2 = 0;
		int idx, factor;

		for (int i = 0; i < enabledTransitions.size(); i++) {
			idx = (int) enabledTransitions.get(i);
			factor = 1;
			// Change factor of exp, if needed (depends on the semantics!)
			if (pnModel.isInfiniteSemantics(idx))
				factor = pnModel.getEnablingDegree(marking, idx);

			aux += factor * pnModel.getTransitionRate(idx);
			v.add(new TransitionEvent(idx, aux2, aux));
			aux2 = aux;
		}
		
		// Check now timed transitions in conflict, if any
		for(int i = 0; i < timedInConflict.size(); i++)
		{
			TimedTransitionConflict auxT = (TimedTransitionConflict) timedInConflict.get(i);
			// XXX Assume single server, by simplicity...
			aux += auxT.superTransition.getRate();
			v.add(new TransitionEvent(auxT.superTransition.getIdx(), aux2, aux));
			aux2 = aux;
		}

		return new TransitionEventList(v, aux);
	}

	/* Generates a random following an exponential distribution of lambda lambda */
	/*
	 * private double expDistribution(double lambda) { return -Math.log(1
	 * -random.nextDouble()) / lambda; }
	 */

	/**
	 * @param simTime
	 *            Maximum of simulation time to reach. If -1 is given, then max
	 *            time is set to 600*(max delay of transitions of the Petri net)
	 * @param confLevel
	 *            Required confidence level to be achieved in the simulation
	 * @param errAcc
	 *            Error accuracy to be achieved in the simulation
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void simulate(double confLevel, double errAcc) {
		System.out.println("Simulating with confidence level " + confLevel
				+ " and error accuracy " + errAcc + "\n(observed transitions: "
				+ this.getObsTransitionsID() + ") [" + getMethod() + "].");

		/* Replication method */
		int minReps = this.getMinNumberOfReplications(), MAX_REPS = this
				.getMaxNumberOfReplications();
		if (MAX_REPS <= 0)
			MAX_REPS = minReps + 80; // At least, 80 times replicated...

		ArrayList[] avgThr = new ArrayList[this.obsTransitions.length];
		double[] mulThr;
    	ObservationResultSim auxSimResult = null;
		for (int i = 0; i < avgThr.length; i++)
			avgThr[i] = new ArrayList(minReps);

		double CONSTSAMPLE = 5, dsample = CONSTSAMPLE
				* (this.pnModel.getSumOfDelays()),
		// dsample =
		// CONSTSAMPLE*(this.getMaxDelay()/this.pnModel.getTransitionsSize()),
		// auxThr,
		alpha = 1 - confLevel;
		double[] estThr = new double[this.obsTransitions.length], error = new double[this.obsTransitions.length];
		/* Init error vector */
		for (int i = 0; i < error.length; i++)
			error[i] = errAcc + 1;

		double simTime = this.getMaxSimulationTime();

		// if(this.getSampleLength() > 0)
		// {
		// dsample = this.getSampleLength();
		// }

		int // lsim,
		msim = new Double(Math.ceil(simTime) / dsample).intValue();
		double // initTime = lsim*dsample,
		finalTime = simTime, t_alpha2;

		if (msim <= 0) {
			msim = 300;
			finalTime = msim * dsample;
		}
		// lsim = msim/8;

		int[] obsIdxs = this.getObsTransitionIdx();

		int nIts = 0;
		long startTime = System.currentTimeMillis();
		boolean accError = true;
		while ((accError || nIts < (minReps - 1)) && (nIts < MAX_REPS)) {
			/* Set to 0 */
			this.firingCounts = new int[nTransitions];
			this.averageMarking = new double[nPlaces]; // FIXME We only take into account the average marking of last iteration...
    		this.lastTimeMarkedPlace = new double[nPlaces];

			/* Simulate */
			// mulThr = runSimulation(dsample, finalTime);
			//mulThr = runSimulation(finalTime);
    		auxSimResult = runSimulation(finalTime);
    		mulThr = auxSimResult.obsTrans;
			if (mulThr == null) // Happens with no evolve from given initial marking
				return;

			for (int i = 0; i < obsIdxs.length; i++) {
				/* Compute average for each observed transition */
				// auxThr = computeMean(mulThr[i], lsim, mulThr[i].size());
				// avgThr[i].add(auxThr);
				avgThr[i].add(mulThr[i]);

				if (nIts >= (minReps - 1)) {
					/* Compute confidence interval for the mean, stdev unknown */
					estThr[i] = computeMean(avgThr[i].toArray());
					t_alpha2 = StudentDist.inverseF(nIts, 1 - alpha / 2);
					error[i] = (computeBiasedStDev(avgThr[i].toArray()) / Math
							.sqrt(avgThr[i].size())) * t_alpha2;
					// System.out.println("Thr: " + estThr + "; error: " +
					// error);
				}
				// Check accuracy
				accError &= error[i] > errAcc;
			}

			nIts++;
		}
		long stopTime = System.currentTimeMillis();
		if (nIts == MAX_REPS && accError) {
			/* Error, max reps reached and no error convergence... */
			System.err
					.println("Warning: simulation does not converge (desired accuracy error not reached), consider to increment number of max. replications.");
		}
		System.out.println("End simulation.");

		/* Set new last result... */
		simResult = new SimulationResult(estThr, auxSimResult.obsPlaces, confLevel, error, nIts,
				(stopTime - startTime));
	}

	private String getObsTransitionsID() {
		StringBuilder s = new StringBuilder();
		int[] aux = this.getObsTransitionIdx();
		s.append("[");
		for (int i = 0; i < aux.length; i++)
			s.append(this.pnModel.getTransitionID(aux[i]) + ", ");

		s.replace(s.length() - 2, s.length(), "]");
		return s.toString();
	}

	/**
	 * Simulate the PN and allows to simulate while observing other transition
	 * different than T_0 (observed by default)
	 * 
	 * @param idxIDTrans
	 *            String labels of new transition to be observed
	 * @param simTime
	 *            Maximum of simulation time to reach. If -1 is given, then max
	 *            time is set to 600*(max delay of transitions of the Petri net)
	 * @param confLevel
	 *            Required confidence level to be achieved in the simulation
	 * @param errAcc
	 *            Error accuracy to be achieved in the simulation
	 */
	public void simulate(String idxIDTrans[], double simTime, double confLevel,
			double errAcc) {
		/* Change transition to observe */
    	this.setObsTransitionsPlaces(getTransitionIndexes(idxIDTrans)); // FIXME Check this, we need to extend this to average marking
		/* Invoke simulation */
		simulate(confLevel, errAcc);
	}

	public void simulate(int idxTrans[], double simTime, double confLevel,
			double errAcc) {
		/* Change transition to observe */
		this.setObsTransitionsPlaces(idxTrans);
		/* Invoke simulation */
		simulate(confLevel, errAcc);
	}

	public void simulate(double simTime, double confLevel, double errAcc) {
		this.setMaxSimulationTime(simTime);
		/* Invoke simulation */
		simulate(confLevel, errAcc);
	}

	public void simulate(String idxIDTrans[], int minReplications,
			double simTime, double confLevel, double errAcc) {
		this.setMinNumberOfReplications(minReplications);
		/* Invoke simulation */
		simulate(idxIDTrans, simTime, confLevel, errAcc);
	}

	public void simulate(String idxIDTrans[], int minReplications,
			int maxReplications, double simTime, double confLevel, double errAcc) {
		this.setMinNumberOfReplications(minReplications);
		this.setMaxNumberOfReplications(maxReplications);
		/* Invoke simulation */
		simulate(idxIDTrans, simTime, confLevel, errAcc);
	}

	public void simulate(int idxTrans[], int minReplications, double simTime,
			double confLevel, double errAcc) {
		this.setMinNumberOfReplications(minReplications);
		/* Invoke simulation */
		simulate(idxTrans, simTime, confLevel, errAcc);
	}

	public void simulate(int idxTrans[], int minReplications,
			int maxReplications, double simTime, double confLevel, double errAcc) {
		this.setMinNumberOfReplications(minReplications);
		this.setMaxNumberOfReplications(maxReplications);
		/* Invoke simulation */
		simulate(idxTrans, simTime, confLevel, errAcc);
	}
	
	 private void setObsTransitionsPlaces(int idxTrans[])
	    {
	    	int aux = idxTrans.length;
	    	for(int i = 0; i < idxTrans.length && aux == idxTrans.length; i++)
	    		if(idxTrans[i] > (this.nTransitions - 1))
	    			aux =  i;

	    	this.setObsTransitionPlacesIdx(idxTrans, aux);
	    }
	
	private int[] getTransitionIndexes(String[] idxIDTrans) {
		int[] idxs = new int[idxIDTrans.length];
		for (int i = 0; i < idxs.length; i++)
			idxs[i] = this.pnModel.getTransitionIndex(idxIDTrans[i]);

		return idxs;
	}

	private void setMaxSimulationTime(double maxTime) {
		if (maxTime > 0) {
			this.maxTime = maxTime;
			System.out.println("Setting maximum simulation time to " + maxTime);
		}
	}

	private double getMaxSimulationTime() {
		return this.maxTime;
	}

	// private void setSampleLength(double sampleLength)
	// {
	// if(sampleLength > 0)
	// {
	// this.sampleLength = sampleLength;
	// System.out.println("Setting sample length to " + sampleLength);
	// }
	// }
	//
	// private double getSampleLength()
	// {
	// return this.sampleLength;
	// }

	private void setMinNumberOfReplications(int minReplications) {
		if (minReplications > 0) {
			this.minReplications = minReplications;
			System.out.println("Setting minimum no. replications: "
					+ minReplications);
		}
	}

	private void setMaxNumberOfReplications(int maxReplications) {
		if (maxReplications > 0) {
			this.maxReplications = maxReplications;
			System.out.println("Setting maximum no. replications: "
					+ maxReplications);
		}
	}

	private int getMinNumberOfReplications() {
		return this.minReplications;
	}

	private int getMaxNumberOfReplications() {
		return this.maxReplications;
	}

	private double computeBiasedStDev(Object[] thr) {
		double sum = 0, mean = computeMean(thr);

		for (int i = 0; i < thr.length; i++)
			sum += Math.pow(((Double) thr[i]).doubleValue() - mean, 2);

		return Math.sqrt(sum / (thr.length - 1));
	}

	private double computeMean(Object[] thr) {
		double sum = 0;

		for (int i = 0; i < thr.length; i++)
			sum += (Double) thr[i];

		return sum / thr.length;
	}

	@SuppressWarnings({ "rawtypes", "unused" })
	private double computeMean(ArrayList thr, int l, int h) {
		double sum = 0;

		for (int i = l; i < h; i++)
			sum += (Double) thr.get(i);

		return sum / (h - l);
	}

	@SuppressWarnings("unused")
	private double getMaxDelay() {
		double max = 1.0 / this.pnModel.getTransitionRate(0), aux;
		for (int i = 1; i < nTransitions; i++) {
			aux = 1.0 / this.pnModel.getTransitionRate(i);
			if (aux > max) {
				max = aux;
			}
		}
		return max;
	}

	/**
	 * Gets the simulation results in a fancy way for printing them
	 * 
	 * @return A string with the simulation results formatted with HTML text
	 */
	public String printResult() {
		/* Get results and formatted it in fancy way... */
		StringBuilder s = new StringBuilder();

		if (simResult == null)
			s.append("<b>No simulation result computed.</b> Check initial marking. It may happen no marking evolution is possible.");
		else {
			s.append("<b>Estimated throughput</b>:<br/>");
			int[] obsIdxs = this.getObsTransitionIdx();
			for (int i = 0; i < obsIdxs.length; i++) {
				s.append("[" + this.pnModel.getTransitionID(obsIdxs[i]) + "]: ");
				s.append(String.format("%.6e (" + Character.toChars(177)[0]
						+ "%.4e%%)<br/>", simResult.thr[i],
						simResult.error[i] * 100));
			}
			s.append("<b>Estimated average marking</b>:<br/>");
			int[] obsPlacesIdx = this.getObsPlaceIdx();
			for(int i = 0; i < obsPlacesIdx.length; i++)
			{
				s.append("[" + this.pnModel.getPlaceID(obsPlacesIdx[i]) + "]: " );
				s.append(String.format("%.6e<br/>", 
							simResult.avgMarking[i]));
			}
			s.append(String.format("<b>Confidence level:</b> %.2f%%<br/>",
					simResult.confLevel * 100));
			s.append(String.format("(simulation time: %.4fs.)<br/>",
					simResult.elapsedTime / 1000.0));

		}
		return s.toString();
	}
	
	public void printResultsToFile(FileWriter f)
	{
		PrintWriter data = new PrintWriter(f);
		
		if(simResult != null)
		{
			// Average throughput
			int[] obsIdxs = this.getObsTransitionIdx();
			for (int i = 0; i < obsIdxs.length; i++) {
				data.append("[" + this.pnModel.getTransitionID(obsIdxs[i]) + "]: ");
				data.append(String.format("%.6e (" + Character.toChars(177)[0]
						+ "%.4e%%)\n", simResult.thr[i],
						simResult.error[i] * 100));
			}
			// Average marking
			int[] obsPlacesIdx = this.getObsPlaceIdx();
			for(int i = 0; i < obsPlacesIdx.length; i++)
			{
				data.append("[" + this.pnModel.getPlaceID(obsPlacesIdx[i]) + "]: " );
				data.append(String.format("%.6e\n", 
							simResult.avgMarking[i]));
			}
		}
		data.close();
	}

	/**
	 * Gets estimated throughput from the simulation result XXX: Warning, it
	 * does not check if there exists some simulation result...
	 * 
	 * @return Estimated throughput from the last simulation result
	 */
	public double[] getEstimatedThr() {
		return simResult.thr;
	}

	/**
	 * Gets elapsed simulation time from the simulation result XXX: Warning, it
	 * does not check if there exists some simulation result...
	 * 
	 * @return Elapsed simulation time from the last simulation result
	 */
	public long getSimulationTime() {
		return simResult.elapsedTime;
	}

	public double[] getSimulationError() {
		return simResult.error;
	}

	/* Auxiliary classes needed by Simulator.java */

	class SimulationResult
	{
		public double[] thr;
		public double[] avgMarking;
		public double confLevel;
		public double[] error;
		public double nIts;
		public long elapsedTime;
		
		public SimulationResult(double thr[], double avgMarking[], double confLevel, double error[], double nIts, long elapsedTime)
		{
			this.thr = thr;
			this.avgMarking = avgMarking;
			this.confLevel = confLevel;
			this.error = error;
			this.nIts = nIts;
			this.elapsedTime = elapsedTime;
		}
	}

	class TransitionEventList {
		public ArrayList<TransitionEvent> al;
		public double sumOfRatios;

		public TransitionEventList(ArrayList<TransitionEvent> al,
				double sumOfRatios) {
			this.sumOfRatios = sumOfRatios;
			this.al = al;
		}
	}

	class TransitionEvent {
		public int idxTransition;
		public double minValue;
		public double maxValue;

		public TransitionEvent(int idxTransition, double minValue,
				double maxValue) {
			this.idxTransition = idxTransition;
			this.minValue = minValue;
			this.maxValue = maxValue;
		}
	}

	class NextEvent {
		public double nextTime;
		public int nextTransition;

		public NextEvent(int nextTransition, double nextTime) {
			this.nextTime = nextTime;
			this.nextTransition = nextTransition;
		}
	}

	@Override
	public ArrayList<String> getResultsHeader() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void computeStrategy() {
		// TODO Auto-generated method stub

	}

	/**
	 * Retuns the index of the observation transition
	 * 
	 * @return
	 */
	public int[] getObsTransitionIdx() {
		return obsTransitions;
	}
	
	public int[] getObsPlaceIdx()
	{
		return obsPlaces;
	}
	
	private void setObsTransitionPlacesIdx(int[] obsTransitionIdx, int pivotIdx)
	{
		int maxTrans = this.pnModel.getTransitionsSize();
    	// Process first the transitions
		if(obsTransitionIdx.length > 0)
    	{
    		boolean valid = true;
    		for(int i = 0; i < pivotIdx && pivotIdx <= obsTransitionIdx.length && valid; i++)
    			valid &= (0 <= obsTransitionIdx[i] && obsTransitionIdx[i] < maxTrans);
    		if(!valid)
    		{
    			System.err.println("Error: some observed transition index not found, setting to [" + this.pnModel.getTransitionID(0) + "]...");
    			this.obsTransitions = new int[1];
    			this.obsTransitions[0] = 0;
    		}
    		else if(pivotIdx != 0)
    		{
    			this.obsTransitions = new int[pivotIdx];
    			for(int i = 0; i < obsTransitions.length && i < pivotIdx; i++)
    				this.obsTransitions[i] = obsTransitionIdx[i];
    		}
    	}else{
    		System.err.println("Not given observed transition, setting to [" + this.pnModel.getTransitionID(0) + "]...");
    		this.obsTransitions = new int[1];
			this.obsTransitions[0] = 0;
    	}
		
		// And now, time for the places...
		int maxPlaces = this.pnModel.getPlacesSize();
		if(obsTransitionIdx.length > 0)
    	{
    		boolean valid = true;
    		for(int i = pivotIdx; i < obsTransitionIdx.length && valid; i++)
    			valid &= (0 <= obsTransitionIdx[i] && (obsTransitionIdx[i] - maxTrans) < maxPlaces);
    		if(!valid)
    		{
    			System.err.println("Error: some observed place index not found, setting to [" + this.pnModel.getPlaceID(0) + "]...");
    			this.obsPlaces = new int[1];
    			this.obsPlaces[0] = 0;
    		}else
    		{
    			this.obsPlaces = new int[obsTransitionIdx.length - pivotIdx];
    			for(int i = 0; i < obsPlaces.length; i++)
    				this.obsPlaces[i] = obsTransitionIdx[i + pivotIdx] - maxTrans;
    		}
    	}
	}
	
	/* Class added for supporting average place marking computation while simulating */
	class ObservationResultSim
	{
		public double[] obsTrans;
		public double[] obsPlaces;
		
		public ObservationResultSim(double[] obsPlaces, double[] obsTrans)
		{
			this.obsPlaces = obsPlaces;
			this.obsTrans = obsTrans;
		}
	}
	
	private double getRateOfListOfTransition(ArrayList<Integer> idxTransitions)
	{
		double accRate = 0;
		
		for(int i = 0; i < idxTransitions.size(); i++)
		{
			accRate += this.pnModel.getTransitionRate(idxTransitions.get(i));
		}
		
		return accRate;
	}
}
