/**
 * Filename: ExactSimulator.java
 * Date: August, 		2014 -- Ricardo J. Rodríguez
 * 
 * Implements an exact method simulator for GSPNs
 * 
 * @author (C) Ricardo J. Rodríguez (University of León, 2014) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */

package pipe.modules.gspnsimulator;

import java.util.ArrayList;

import pipe.modules.bounds.dataLayer.PetriNetModel;

public class ExactSimulator extends Simulator {

	public ExactSimulator(PetriNetModel pnModel) {
		super(pnModel);
	}

	@Override
	protected double runSimulationMethod(int[] marking, double maxSimulation,
			double[] obsTrans) {
		double simTime = 0.0// ,
				// formerTime = simTime
				;
			// ArrayList[] obsTrans = new ArrayList[this.obsTransitions.length];
			// for(int i = 0; i < obsTrans.length; i++)
			// obsTrans[i] = new ArrayList(new
			// Double(Math.ceil(maxSimulation/dsample)).intValue());
		ArrayList<Simulator.TimedTransitionConflict> timedInConflict 
							= new ArrayList<Simulator.TimedTransitionConflict>(1);	

		TransitionEventList aux = null;
		/* Fire immediate transitions until no more can be fired */
		marking = fireImmediateTransitionsAt(marking, simTime);
		/* Get enabled transitions and looks for next event... */
		aux = generateEventList(marking,
				getEnabledTransitionsAt(TransitionType.TIMED_TRANS, marking, timedInConflict),
				timedInConflict);
		if (aux.al.size() == 0) {
			System.err
			.println("Check initial marking. No evolving is possible.");
			return 0;
		}

		NextEvent ne = getNextEvent(aux);
		// int auxIdx = 0;
		
		do {
			/* Get next event */
			simTime += ne.nextTime;
			/* Fires next transition */
			marking = firesTransitionAt(ne.nextTransition, marking, 1, simTime, timedInConflict);
			
			// /* Observation */
			// if(simTime >= formerTime + dsample)
			// {
			// //int auxIdx = new Double(Math.ceil(simTime/dsample)).intValue()
			// - 1;
			// //System.out.println("Observation " + auxIdx);
			// for(int i = 0; i < obsTrans.length; i++)
			// {
			// obsTrans[i].add(this.firingCounts[this.obsTransitions[i]]/simTime);
			// // Take observation
			// //thrObs.add(this.firingCounts[idxObsTransitions[i]]/simTime); //
			// Take observation
			// }
			//
			// formerTime = simTime;
			// }

			/* Fire immediate transitions until no more can be fired */
			marking = fireImmediateTransitionsAt(marking, simTime);

			/* Get enabled transitions and looks for next event... */
			timedInConflict = new ArrayList<Simulator.TimedTransitionConflict>(1);
			aux = generateEventList(
					marking,
					getEnabledTransitionsAt(TransitionType.TIMED_TRANS, marking, timedInConflict),
					timedInConflict);
			if (aux.al.size() == 0) {
				System.err
				.println("Ups... It seems dead marking was reached. No evolving is possible. Exiting!");
				return 0;
			}
			ne = getNextEvent(aux);
		} while (simTime <= maxSimulation);

		/* End of simulation... */
		return simTime;
	}

	@Override
	protected SimulationMethod getMethod() {
		return SimulationMethod.EXACT_SIM;
	}

}
