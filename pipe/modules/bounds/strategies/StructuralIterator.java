/**
 * Filename: StructuralIterator.java
 * Date: January, 2012 -- first release
 * 
 * Implements the structural marking at a place
 * or structural enabling at a transition
 * 
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */
package pipe.modules.bounds.strategies;

import java.util.ArrayList;

import pipe.modules.bounds.dataLayer.PetriNetModel;
import pipe.modules.bounds.errors.ResultException;
import pipe.modules.bounds.lpp.LPResult;
import pipe.modules.bounds.lpp.solvers.SolverGLPK;
import pipe.modules.bounds.lpp.structural.InitialMarkingSIP;
import pipe.modules.bounds.lpp.structural.StructuralEnabling;
import pipe.modules.bounds.lpp.structural.StructuralImplicitPlace;
import pipe.modules.bounds.lpp.structural.StructuralLPP;
import pipe.modules.bounds.lpp.structural.StructuralMarking;

public class StructuralIterator extends Strategy {
	private StructuralType sType;

	/**
	 * Default constructor. 
	 * 
	 * @param pnModel Petri net model to work with
	 */
	public StructuralIterator(PetriNetModel pnModel)
	{
		super(pnModel);
	}
	
	/**
	 * Sets the type of structural computation to perform,
	 * either structural marking at a place or structural enabling at a transition,
	 * and the Petri net model to work with
	 * 
	 * @param sType Structural type computation
	 */
	public void setStructuralType(StructuralType sType)
	{
		this.sType = sType;
	}
	
	/**
	 * Computes the structural marking or structural enabling for the element
	 * with index given as parameter
	 * @param idxItem Index of the element
	 */
	public void computeOneItem(int idxItem)
	{
		StructuralLPP sb = null;
		boolean isSIP = false;
		
		switch(sType)
		{
			case STRUCTURAL_MARKING:
				sb = new StructuralMarking(idxItem);
				break;
			case STRUCTURAL_ENABLING:
				sb = new StructuralEnabling(idxItem);
				break;
			case STRUCTURAL_IMPLICIT_PLACE:
				sb = new StructuralImplicitPlace(idxItem);
				isSIP = true;
		}
		
		sb.setSolver(new SolverGLPK());
		sb.setPetriNet(pnModel);
		sb.solve();
		
		result = new LPResult[1];
		/* Check SIPs... */
		if(isSIP)
		{
			// Compute now initial marking to make such an SIP an implicit place
			InitialMarkingSIP imsip = new InitialMarkingSIP(idxItem);
			imsip.setSolver(new SolverGLPK());
			imsip.setPetriNet(pnModel);
			imsip.solve();
			
			/* Get second results, and build results */
			try
			{
				if(imsip.getResult() != null)
					result[0] = new LPResult(imsip.getResult().getResult(), sb.getResult().getVariables());
			}
			catch (ResultException e)
			{
				e.printStackTrace();
			}	
		}
		else
		{
			/* Get results... */
			result[0] = sb.getResult();
		}
	}
	
	/**
	 * Computes the structural marking or structural enabling for 
	 * all elements in the Petri net
	 */
	public void computeAllItems()
	{
		switch(sType)
		{
			case STRUCTURAL_MARKING:
				/* Get results... */
				result = new LPResult[pnModel.getPlacesSize()];
				for(int i = 0; i < result.length; i++)
				{
					StructuralMarking smb = new StructuralMarking(i);
					smb.setSolver(new SolverGLPK());
					smb.setPetriNet(pnModel);
					smb.solve();
					/* Get results... */
					result[i] = smb.getResult();
				}
				break;
			case STRUCTURAL_ENABLING:
				/* Get results... */
				result = new LPResult[pnModel.getTransitionsSize()];
				for(int i = 0; i < result.length; i++)
				{
					StructuralEnabling seb = new StructuralEnabling(i);
					seb.setSolver(new SolverGLPK());
					seb.setPetriNet(pnModel);
					seb.solve();
					/* Get results... */
					result[i] = seb.getResult();
				}
				break;
			case STRUCTURAL_IMPLICIT_PLACE:
				/* Get results... */
				result = new LPResult[pnModel.getPlacesSize()];
				for(int i = 0; i < result.length; i++)
				{
					StructuralImplicitPlace sip = new StructuralImplicitPlace(i);
					sip.setSolver(new SolverGLPK());
					sip.setPetriNet(pnModel);
					sip.solve();
					
					if(sip.getResult() != null)
					{
						// Compute now initial marking to make such an SIP an implicit place
						InitialMarkingSIP imsip = new InitialMarkingSIP(i);
						imsip.setSolver(new SolverGLPK());
						imsip.setPetriNet(pnModel);
						imsip.solve();
						
						/* Get second results, and build results */
						try
						{
							result[i] = new LPResult(imsip.getResult().getResult(), sip.getResult().getVariables());
						}
						catch (ResultException e)
						{
							e.printStackTrace();
						}
					}
				}
				break;
		}
	}
	
	@Override
	public ArrayList<String> getResultsHeader()
	{
		ArrayList<String> l = new ArrayList<String>();
		
		/* Create header */
		switch(sType)
		{
			case STRUCTURAL_MARKING:
				l.add("Place");
				l.add("Marking bound");
				break;
			case STRUCTURAL_ENABLING:
				l.add("Transition");
				l.add("Enabling bound");
				break;
			case STRUCTURAL_IMPLICIT_PLACE:
				l.add("SIPs Places");
				l.add("m0 to be implicit");
				break;
		}
		
		return l;
	}
	
	public double getResultValue(int idx)
	{
		return result[idx].getResult()[0];
	}

	@Override
	protected void computeStrategy()
	{
		/* do nothing... */		
	}

	public StructuralType getStructuralTypeSelected() {
		return this.sType;
	}
}
