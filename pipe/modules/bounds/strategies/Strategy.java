/**
 * Filename: Strategy.java
 * Date: January, 2012 -- first release
 * 
 * Abstract class Strategy, implements what an strategy should
 * implement
 *
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */
package pipe.modules.bounds.strategies;

import java.util.ArrayList;

import pipe.gui.widgets.ResultsHTMLPane;
import pipe.modules.bounds.dataLayer.PetriNetModel;
import pipe.modules.bounds.lpp.LPResult;

public abstract class Strategy
{
	protected PetriNetModel pnModel;
	protected LPResult[] result;
	private long elapsedTime;
	
	public Strategy(PetriNetModel pnModel) {
		this.pnModel = pnModel;
	}

	/**
	 * Returns an array list of the results header, for printing it in a fancy way
	 * @return An ArrayList<String> with the results header for printing
	 */
	public abstract ArrayList<String> getResultsHeader();
	
	/**
	 * Implements the algorithm of the current strategy
	 */
	protected abstract void computeStrategy();

	/**
	 * Invokes the strategy and reports elapsed time
	 */
	public void compute()
	{
		long initTime = System.currentTimeMillis();
		computeStrategy();
		long stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - initTime;
	}

	/**
	 * Reformats the LP result as an array list, needed for correct printing
	 * @param result The LP result
	 * @return An ArrayList<String> with the results formatted
	 */
	private ArrayList<String> getResults(LPResult[] result)
	{
		ArrayList<String> l = getResultsHeader();
		
		if(result.length == 0 || (result.length != 0 && result[0] == null))
		{
			System.out.println("No result defined¿?");
			return l;
		}
		
		/* Create data output */
		for(int i = 0; i < result.length; i++)
			if(result[i] != null)
				for(int j = 0; j < result[i].getVariables().length; j++)
				{
					l.add(result[i].getVariables()[j]);
					l.add(String.valueOf(result[i].getResult()[j]));
				}
		
		return l;
	}
	
	/**
	 * Prints results in a fancy way
	 * @param result
	 * @return An string with the result put in a fancy way
	 */
	public String printResult(LPResult[] result)
	{
		if(result == null)
			return "No result given.";
		else
		{
			StringBuilder s = new StringBuilder();
			s.append(String.format("Elapsed time (seconds): %.4f", elapsedTime/1000.0));
			return ResultsHTMLPane.makeTable(getResults(result).toArray(), 2, false, true, true, true) + s.toString();
		}
	}
	
	/**
	 * Prints the results in a fancy way
	 * @return An string with the result put in a fancy way
	 */
	public String printResult()
	{
		return printResult(this.result);
	}
	
	/**
	 * Prints array in a fancy way
	 * @param array to be printed
	 * @return An string with the array put in a fancy way
	 */
	public String printResult(Object[] array)
	{
		if(array == null)
			return "No result given.";
		else
			return ResultsHTMLPane.makeTable(array, 2, false, true, true, true);
	}
	
	/**
	 * Prints array in a fancy way
	 * @param array to be printed
	 * @param nColumn number of columns data in array
	 * @return An string with the array put in a fancy way
	 */
	public String printResult(Object[] array, int nColumn)
	{
		if(array == null)
			return "No result given.";
		else
			return ResultsHTMLPane.makeTable(array, nColumn, false, true, true, true);
	}
	
	/**
	 * Gets the current PN model with which the strategy works with
	 * @return the current PN model
	 */
	public PetriNetModel getCurrentPNModel()
	{
		return this.pnModel;
	}
}
