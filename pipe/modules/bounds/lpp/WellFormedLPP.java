/**
 * Filename: WellFormedLPP.java
 * Date: January, 2012 -- first release
 * Describes a Linear Programming problem
 * Besides, creates a Template Method pattern, where some steps are delegated to subclasses
 * for solving the LP problem.
 * 
 * It uses the JavaILP library for solving LP problems
 * 
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */
package pipe.modules.bounds.lpp;

import net.sf.javailp.Problem;
import net.sf.javailp.Result;
import pipe.modules.bounds.dataLayer.PetriNetModel;
import pipe.modules.bounds.errors.ErrorConstants;
import pipe.modules.bounds.errors.ErrorMessage;
import pipe.modules.bounds.lpp.solvers.Solver;

public abstract class WellFormedLPP {
	/* Data layer */
	protected PetriNetModel petriNet;
	protected int	nPlaces = -1,
					nTransitions = -1;
	
	/* LP solver */
	private Solver solver;
	
	/* LP Problem */
	private Problem problem;
	
	/* Result */
	private LPResult result;
	private static double tolerance = 1e-6; /* Tolerance for the LP solver, 1e-6 by default */
	
	/* Deferred steps to subclasses */
	/**
	 * Checks initial conditions to be fulfilled by the PN model
	 * needed by the current LP problem
	 * @return != 0 if there is some error, 0 otherwise
	 */
	protected abstract int checkOtherErrors(); 
	
	/**
	 * Creates the constraints needed to be fulfilled in the LP problem
	 * @return A vector of LPConstraint
	 */
	protected abstract LPConstraint[] loadConstraints();
	
	/**
	 * Creates the objective function of the LP problem
	 * @return A LPObjectiveFunc
	 */
	protected abstract LPObjectiveFunc loadObjectiveFunction();
	
	/**
	 * Creates a vector defining the type of each variable which appears
	 * in the LP problem
	 * @return A vector assigning to each variable its class type
	 */
	protected abstract LPVariable[] loadVariablesType();
	
	/**
	 * Once the LP problem has been solved, then the result
	 * is transformed and processed, depending on the LP solved
	 * @param result Result of the LP to be processed
	 * @return An LPResult 
	 */
	protected abstract LPResult processResult(Result result);
	
	/**
	 * Creates a vector defining upper bounds, if any,
	 * for variables in the LP problem
	 * @return A vector containing upper bounds for LP variables
	 */
	protected abstract LPVarBound[] getVarUpperBounds();
	
	/**
	 * Creates a vector defining lower bounds, if any,
	 * for variables in the LP problem
	 * @return A vector containing lower bounds for LP variables
	 */
	protected abstract LPVarBound[] getVarLowerBounds();
	
	/* Validate data */
	private int checkErrors()
	{
		if(this.petriNet == null)
			return ErrorMessage.showErrorMessage(ErrorConstants.NO_PNMODEL_DEFINED);
		else if(this.solver == null)
			return ErrorMessage.showErrorMessage(ErrorConstants.NO_SOLVER_DEFINED);
		else if(this.petriNet != null &&
				this.petriNet.getPlacesSize() == 0 &&
				this.petriNet.getTransitionsSize() == 0)
			return ErrorMessage.showErrorMessage(ErrorConstants.PN_DEFINED_EMPTY);
		
		return checkOtherErrors();
	}
	
	private void setProblemUpperBounds()
	{
		LPVarBound[] l = getVarUpperBounds();
		
		if(l != null)
			for(LPVarBound aux : l)
				problem.setVarUpperBound(aux.getVariable(), aux.getValue());
	}
	
	private void setProblemLowerBounds()
	{
		LPVarBound[] l = getVarLowerBounds();
		
		if(l != null)
			for(LPVarBound aux : l)
				problem.setVarLowerBound(aux.getVariable(), aux.getValue());
	}
	
	
	/**
	 * Solves the LP problem. Before invoking, LP solver and PN model must be assigned
	 * It follows a TemplateMethod pattern, there exist several steps in the algorithm
	 * deferred to the subclasses
	 * @return > 0 if some error arises in the PN model, -1 if the LP solver does not converge, 0 otherwise
	 */
	public int solve()
	{
		/* Check possible null references */
		int error = checkErrors();
		if(error != 0)
			return error;
			
		
		problem = new Problem();
		
		LPConstraint	constraints[] = loadConstraints();
		LPObjectiveFunc objectiveFunc = loadObjectiveFunction();
		
		LPVariable problemVars[] = loadVariablesType();
		
		/* Set objective function */
		problem.setObjective(objectiveFunc.getFunction(), objectiveFunc.getObjective());
		
		/* Load constraints */
		for(LPConstraint constraint : constraints)
		{		
			problem.add(constraint.getEquation(), constraint.getSign(), constraint.getValue());
			//System.out.println(constraint.getEquation() + constraint.getSign() + constraint.getValue());
		}
		
		/* Set variable types */
		for(LPVariable variable : problemVars)
		{
			problem.setVarType(variable.getVariable(), variable.getVarType());
		}
		
		/* Set upper and lower bounds */
		this.setProblemUpperBounds();
		this.setProblemLowerBounds();
		
		//System.out.println(this);
		
		/* Solve LPP */
		Result result = solver.solve(problem);
		//System.out.println(result);

		/* TODO: How we know an improper execution? */
		if(result == null)
			return -1;
		
		/* Last step: process result */
		this.result = processResult(result);
		
		return error;
	}
	
	/**
	 * Returns the assigned PN model
	 * @return PN model
	 */
	public PetriNetModel getPetriNet() {
		return petriNet;
	}
	
	/**
	 * Sets petriNet as the PN for this LP problem. 
	 * Recomputes again number of places and transitions
	 * @param petriNet New PN to be assigned
	 */
	public void setPetriNet(PetriNetModel petriNet) {
		this.petriNet = petriNet;
		this.nPlaces = petriNet.getPlacesSize();
		this.nTransitions = petriNet.getTransitionsSize();
	}
	
	/**
	 * Get the current LP solver
	 * @return A LP solver class
	 */
	public Solver getSolver() {
		return solver;
	}
	
	/**
	 * Sets the sover as the new LP solver for this LP problem
	 * @param solver New solver to be assigned
	 */
	public void setSolver(Solver solver) {
		this.solver = solver;
	}
	
	/**
	 * Gets the result of the current LP problem solved. 
	 * Previously it should be solve, if not, an error message arises
	 * @return Result of the LP problem
	 */
	public LPResult getResult() {
		if(result == null)
			System.err.println("Check if you have solve the LPP, no result has been given!!");
		return result;
	}
	
	/**
	 * Prints as String the LP problem
	 */
	public String toString()
	{		
		return problem.toString();
	}
	
	/**
	 * Gets the tolerance of the LP problem
	 * @return Tolerance
	 */
	public static double getTolerance() {
		return tolerance;
	}
	
	/**
	 * Sets the new tolerance for the current LP problem
	 * @param tolerance New tolerance to set
	 */
	public void setTolerance(double tolerance) {
		WellFormedLPP.tolerance = tolerance;
	}
}
