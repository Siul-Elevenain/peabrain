/**
 * Filename: LPObjectiveFunc.java
 * Date: January, 2012 -- first release
 * Describes a Linear Programming objective function
 * Example:
 *    max 2*x + y
 *    
 *     Linear aux = new Linear();
 *     aux.add(2, "x"); aux.add(1, "y");
 *     LPObjectiveFunc obj = new LPObjectiveFunc(aux, OptType.MAX);
 * 
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>. 
 */
package pipe.modules.bounds.lpp;

import net.sf.javailp.Linear;
import net.sf.javailp.OptType;

public class LPObjectiveFunc {
	private OptType objective;
	private Linear function;
	
	/**
	 * Constructor for a Linear Programming objective function
	 * @param function Equation to maximise or minimise
	 * @param objective OptType.MAX or OptType.MIN
	 */
	public LPObjectiveFunc(Linear function, OptType objective)
	{
		this.setFunction(function);
		this.setObjective(objective);
	}
	
	/**
	 * Gets objective (MAX or MIN)
	 * @return OptType objective
	 */
	public OptType getObjective() {
		return objective;
	}
	private void setObjective(OptType objective) {
		this.objective = objective;
	}
	
	/**
	 * Gets the objective function
	 * @return A Linear equation
	 */
	public Linear getFunction() {
		return function;
	}
	private void setFunction(Linear function) {
		this.function = function;
	}
}
