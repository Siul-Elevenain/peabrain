/**
 * Filename: LowerThroughputBound.java
 * Date: January, 2012 -- first release
 * Defines constraints for computing lower bound of a transition of a PN model
 * 
 *   m = m0 + C·\sigma (reachability)
 *   sum(t \in \preset{p}) X(t)·Post(p, t) = sum(t \in \postset{p}) X(t)·Pre(p, t), \forall p \in P (token flow)
 *   
 *   \forall t in T, \preset{t} = {p}
 *     x(t)·s(t) >= (m(p) - Pre(p, t) + 1)/ Pre(p, t)
 *   \forall t in T, \preset{t} = {p_1, p_2}, where smb(p_1) < smb(p_2) (structural marking bound)
 *     x(t)·s(t)·Pre(p_1, t) >=
 * 							m(p_1) - Pre(p_1, t) + 1 - smb(p_1)·(1 - (m(p_2)- Pre(p_2, t) + 1)/(smb(p_2) - Pre(p_2, t) + 1))
 * 
 * XXX: Assume non conflict -> otherwise, more constraints should be added (in superclass)
 * 
 * See J. Campos and M. Silva, 
 * "Embedded Product-Form Queueing Networks and the Improvement of Performance Bounds for Petri Net Systems," 
 * Performance Evaluation, vol. 18, iss. 1, pp. 3-19, 1993.
 * http://webdiis.unizar.es/CRPetri/papers/jcampos/93_CS_PE.ps.gz
 * 
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */
package pipe.modules.bounds.lpp.timed.performance;


import net.sf.javailp.Linear;
import net.sf.javailp.OptType;
import pipe.modules.bounds.dataLayer.PetriNetModel;
import pipe.modules.bounds.lpp.LPObjectiveFunc;

public class LowerThroughputBound extends GeneralBound {
	
	/**
	 * Default constructor, needs PN model to compute its lower performance bound
	 * and the index of the transition for being computed to.
	 * 
	 * @param petriNet
	 * @param idxTransition Index of transition to compute its lower performance bound
	 */
	public LowerThroughputBound(PetriNetModel petriNet, int idxTransition)
	{
		super(petriNet, idxTransition);
	}
	
	@Override
	protected LPObjectiveFunc loadObjectiveFunction() {
		LPObjectiveFunc objFunc;
		
		Linear linear = new Linear();
		linear.add(1, TAG_THR + idxTransition);
		
		/* Set objective function*/
		objFunc = new LPObjectiveFunc(linear, OptType.MIN);
		
		return objFunc;
	}
	

}
