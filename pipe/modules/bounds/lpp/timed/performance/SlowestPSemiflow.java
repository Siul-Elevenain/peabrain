/**
 * Filename: SlowestPSemiflow.java
 * Date: January, 2012 -- first release
 * 
 * Computes the slowest P-semiflow of a PN system. The system has to fulfill a set
 * of conditions:
 *  - it must be structurally live
 *  - it must be structurally limited
 *  - Visit ratio must have a unique solution (we only check this one, more easiest...)
 *  - it must have a home state
 *  
 *  It solves the following LP problem:
 *  
 * max Y'·PRE·D
 *   s.t. Y'·C = 0
 *        Y'·M0 = 1
 *        Y >= 0
 *        
 * See J. Campos and M. Silva, 
 * "Embedded Product-Form Queueing Networks and the Improvement of Performance Bounds for Petri Net Systems," 
 * Performance Evaluation, vol. 18, iss. 1, pp. 3-19, 1993.
 * http://webdiis.unizar.es/CRPetri/papers/jcampos/93_CS_PE.ps.gz 
 * 
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */

package pipe.modules.bounds.lpp.timed.performance;


import net.sf.javailp.Linear;
import net.sf.javailp.OptType;
import net.sf.javailp.Result;
import pipe.modules.bounds.lpp.LPConstants;
import pipe.modules.bounds.lpp.LPConstraint;
import pipe.modules.bounds.lpp.LPObjectiveFunc;
import pipe.modules.bounds.lpp.LPResult;
import pipe.modules.bounds.lpp.LPVarBound;
import pipe.modules.bounds.lpp.LPVariable;
import pipe.modules.bounds.lpp.structural.VisitRatio;
import pipe.modules.bounds.errors.ErrorConstants;
import pipe.modules.bounds.errors.ResultException;

public class SlowestPSemiflow extends PerformanceLPP {
	public final String TAG_PCOMPONENT = "y";
	
	@Override
	public int checkOtherErrors() {
		/* Visit ratio of transition has to be a unique solution... */
		VisitRatio vr = new VisitRatio(this.petriNet, 0);
		if(!vr.hasUniqueSolution())
			return ErrorConstants.NO_UNIQUESOLUTION_VISITRATIO;
		
		/* XXX: Warning, more constraints must be fulfilled in order this result has some meaning... */
		return 0;
	}

	@Override
	protected LPConstraint[] loadConstraints() {
		LPConstraint[] constraints = new LPConstraint[nPlaces + 1 + nTransitions];
		
		/* Create inequality constraints */
		for(int i = 0; i < nPlaces; i++)
		{
			Linear linear = new Linear();
			linear.add(1, TAG_PCOMPONENT + i);
			
			constraints[i] = new LPConstraint(linear, LPConstants.GREATEST_THAN_OR_EQUAL, 0);
		}
		
		/* Create equality constraints */
		/* Y'·m0 = 1*/
		Linear auxLinear = new Linear();
		for(int i = 0; i < nPlaces; i++)
			if(this.petriNet.isInitialMarked(i))
				auxLinear.add(this.petriNet.getInitialMarkingAtPlace(i), TAG_PCOMPONENT + i);
		
		constraints[nPlaces] = new LPConstraint(auxLinear, LPConstants.EQUAL, 1);
		
		/* Y'·C = 0 */
		int[][] C = this.petriNet.getIncidenceMatrix();
		for(int j = 0; j < nTransitions; j++)
		{
			auxLinear = new Linear();
			for(int i = 0; i < nPlaces; i++)
			{
				if(C[i][j] != 0)
				{
					auxLinear.add(C[i][j], TAG_PCOMPONENT + i);
				}
			}
			constraints[(nPlaces + 1) + j] = new LPConstraint(auxLinear, LPConstants.EQUAL, 0);
		}
		
		return constraints;
	}


	@Override
	protected LPObjectiveFunc loadObjectiveFunction() {
		LPObjectiveFunc objFunc;
		
		/* Compute D = s·v */
		double[] D = new double[nTransitions];
		for(int i = 0; i < D.length; i++)
			if(!this.petriNet.isImmediate(i))
				D[i] = (1/this.petriNet.getTransitionRate(i))*this.petriNet.getVisitRatioAtTransition(i);
		
		/* Compute Pre·D */
		double[] PreD = new double[nPlaces];
		for(int i = 0; i < nPlaces; i++)
			for(int j = 0; j < nTransitions; j++)
				PreD[i] += this.petriNet.getPre()[i][j]*D[j];
		
		/* Y·Pre·D */
		Linear linear = new Linear();
		for(int i = 0; i < nPlaces; i++)
			linear.add(PreD[i],  TAG_PCOMPONENT + i);
		
		/* Set objective function*/
		objFunc = new LPObjectiveFunc(linear, OptType.MAX);
		
		return objFunc;
	}

	@Override
	protected LPVariable[] loadVariablesType() {
		LPVariable[] vars = new LPVariable[nPlaces];
		
		for(int i = 0; i < nPlaces; i++)
		{
			vars[i] = new LPVariable(TAG_PCOMPONENT + i, Double.class);
		}
		return vars;
	}

	@Override
	protected LPResult processResult(Result result) {
		/* Process LP result */
		LPResult finalResult = null;
		
		double[] auxD = new double[nPlaces + 1];
		String[] aux2 = new String[nPlaces + 1];
		
		/* First one, the thr */
		auxD[0] = result.getObjective().doubleValue();
		aux2[0] = "Thr.";
		
		for(int i = 0; i < nPlaces; i++)
		{
			aux2[i + 1] = this.petriNet.getPlaceID(i);
			auxD[i + 1] = result.get(TAG_PCOMPONENT + i).doubleValue();
		}
		
		try {
			finalResult = new LPResult(auxD, aux2);
		} catch (ResultException e) {
			e.printStackTrace();
		}
				
		return finalResult;
	}

	@Override
	protected LPVarBound[] getVarUpperBounds() {
		/* No bounds needed */
		return null;
	}

	@Override
	protected LPVarBound[] getVarLowerBounds() {
		/* No bounds needed */
		return null;
	}

}
