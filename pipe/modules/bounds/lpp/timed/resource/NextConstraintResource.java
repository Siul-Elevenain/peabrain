/**
 * Filename: NextConstraintResource.java
 * Date: January, 2012 -- first release
 * 
 * Defines an LP problem for computing, for a given PN, and a given resources,
 * the next resource which is constraining the system, in performance terms.
 * 
 * It solves the following LP problem:
 *   min sum(a_j), j \in 1..k
 *    s.t.
 *          Y_(k + 1)'·PRE·D = Y'·PRE·D
 *          Y_(k + 1)·C = 0
 *        	Y_(k + 1)'·M0 = Y_j'·M0, j \in 1..k
 *          Y_(k + 1)(p) >= 0, p \in P \ {p_a_j}, j \in 1..k
 *          Y_(k + 1)(p) = 0,  p \in {p_a_j}, j \in 1..k
 *          a_j >= 0, j \in 1..k
 *          
 *   where Y is the slowest p-semiflow in the system, 
 * 
 * TODO: Add reference to the paper, when published
 *  
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>. 
 */

package pipe.modules.bounds.lpp.timed.resource;

import java.util.ArrayList;

import net.sf.javailp.Linear;
import net.sf.javailp.OptType;
import net.sf.javailp.Result;
import pipe.modules.bounds.dataLayer.PetriNetModel;
import pipe.modules.bounds.errors.ErrorConstants;
import pipe.modules.bounds.errors.ResultException;
import pipe.modules.bounds.lpp.LPConstants;
import pipe.modules.bounds.lpp.LPConstraint;
import pipe.modules.bounds.lpp.LPObjectiveFunc;
import pipe.modules.bounds.lpp.LPResult;

import pipe.modules.bounds.lpp.LPVarBound;
import pipe.modules.bounds.lpp.LPVariable;
import pipe.modules.bounds.lpp.structural.VisitRatio;

public class NextConstraintResource extends ResourceLPP
{
	private String TAG_RESOURCE = "alpha";
	private String TAG_PCOMPONENT = "y";
	
	private int nResources;
	private int[] idxResources;
	private double[][] yValues;
	
	/**
	 * Default constructor. It needs the PN model to compute, the index of resources 
	 * and the computed p-semiflow for each resource
	 * 
	 * @param petriNet PN model
	 * @param idxResources Vector if resource indexes
	 * @param yValues Computed values for each p-semiflow
	 * @throws Exception if given p-component values does not match either the number of resources or number of places
	 */
	public NextConstraintResource(PetriNetModel petriNet, int[] idxResources, double[][] yValues) throws Exception
	{
		super.setPetriNet(petriNet);
		this.nResources = idxResources.length; // k
		this.idxResources = idxResources;
		this.yValues = yValues; // y_j, j \in 1..k
		if(yValues.length != nResources && yValues[0].length != nPlaces)
			throw new Exception("The given yValues does not match either the number of resources or number of places.");
	}

	
	@Override
	protected int checkOtherErrors() {
		/* Visit ratio of transition has to be a unique solution... */
		VisitRatio vr = new VisitRatio(this.petriNet, 0);
		if(!vr.hasUniqueSolution())
			return ErrorConstants.NO_UNIQUESOLUTION_VISITRATIO;
		
		/* XXX: Warning, more constraints must be fulfilled in order this result has some meaning... */
		return 0;
	}

	@Override
	protected LPConstraint[] loadConstraints() {
		LPConstraint[] constraints = new LPConstraint[nTransitions + 1 + nResources*3 + nPlaces - idxResources.length];
		
		/* Create equality constraints */
		/* Y_(k + 1)'·C = 0 */
		Linear auxLinear;
		int[][] C = this.petriNet.getIncidenceMatrix();
		for(int j = 0; j < nTransitions; j++)
		{
			auxLinear = new Linear();
			for(int i = 0; i < nPlaces; i++)
			{
				if(C[i][j] != 0)
				{
					auxLinear.add(C[i][j], TAG_PCOMPONENT + i);
				}
			}
			constraints[j] = new LPConstraint(auxLinear, LPConstants.EQUAL, 0);
		}
		
		/* y_(k+1)·Pre·D = y_1·Pre·D */
		/* Compute D = s·v */
		double[] D = new double[nTransitions];
		for(int i = 0; i < D.length; i++)
			D[i] = (1/this.petriNet.getTransitionRate(i))*this.petriNet.getVisitRatioAtTransition(i);
		
		/* Compute Pre·D */		
		double[] PreD = new double[nPlaces];
		for(int i = 0; i < nPlaces; i++)
			for(int j = 0; j < nTransitions; j++)
				PreD[i] += this.petriNet.getPre()[i][j]*D[j];
		
		/* Y·Pre·D */
		auxLinear = new Linear();
		for(int i = 0; i < nPlaces; i++)
			if(PreD[i] > getTolerance())
				auxLinear.add(PreD[i],  TAG_PCOMPONENT + i);
		
		/* Compute y_1·Pre·D */
		double y1PreD = 0;
		for(int i = 0; i < nPlaces; i++)
			y1PreD += yValues[0][i]*PreD[i];
		
		/* Add constraint */
		constraints[nTransitions] = new LPConstraint(auxLinear, LPConstants.EQUAL, y1PreD);
		
		/* y_(k+1)(r_j) = 0, j \in 1..k */
		for(int i = 0; i < nResources; i++)
		{
			auxLinear = new Linear();
			auxLinear.add(1,  TAG_PCOMPONENT + idxResources[i]);
			constraints[(nTransitions + 1) + i] = new LPConstraint(auxLinear, LPConstants.EQUAL, 0);
		}
		
		/* y_(k+1)·m0^\Delta = y_j·m0^\Delta, j \in 1..k */
		ArrayList<Integer> v = this.petriNet.getPlacesMarkedAtInitialMarking();
		for(int r = 0; r < nResources; r++)
		{
			double aux = 0;
			int idx;
			auxLinear = new Linear();
			for(int i = 0; i < v.size(); i++)
			{
				idx = ((Integer)v.get(i)).intValue();
				auxLinear.add(this.petriNet.getInitialMarkingAtPlace(idx), TAG_PCOMPONENT + idx);
				if(yValues[r][idx] > getTolerance())
					auxLinear.add(-yValues[r][idx], TAG_RESOURCE + r);
				aux += this.petriNet.getInitialMarkingAtPlace(idx)*yValues[r][idx];
			}
			constraints[(nTransitions + 1) + nResources + r] = new LPConstraint(auxLinear, LPConstants.EQUAL, aux);
		}
		int idxC = (nTransitions + 1) + nResources*2;
		
		boolean[] isResource = new boolean[nPlaces];
		for(int i = 0; i < idxResources.length; i++)
		{
			isResource[idxResources[i]] = true;
		}
		
		/* Create inequality constraints */
		int aux = 0;
		for(int i = 0; i < nPlaces; i++)
		{
			if(isResource[i])
				continue; // Avoid this constraint if it's a resource
			
			Linear linear = new Linear();
			linear.add(1, TAG_PCOMPONENT + i);
			
			constraints[aux + idxC] = new LPConstraint(linear, LPConstants.GREATEST_THAN_OR_EQUAL, 0);
			aux++;
		}
		idxC += (nPlaces - idxResources.length);
		
		/* r_j >= 0, j \in 1..k */
		for(int i = 0; i < nResources; i++)
		{
			Linear linear = new Linear();
			linear.add(1, TAG_RESOURCE + i);
			
			constraints[i + idxC] = new LPConstraint(linear, LPConstants.GREATEST_THAN_OR_EQUAL, 0);
		}
		
		return constraints;
	}

	@Override
	protected LPObjectiveFunc loadObjectiveFunction() {
		LPObjectiveFunc objFunc;
		
		Linear linear = new Linear();
		for(int i = 0; i < nResources; i++)
			linear.add(1,  TAG_RESOURCE + i);
		
		/* Set objective function*/
		objFunc = new LPObjectiveFunc(linear, OptType.MIN);
		
		return objFunc;
	}

	@Override
	protected LPVariable[] loadVariablesType() {
		LPVariable[] vars = new LPVariable[nPlaces + nResources];
		
		for(int i = 0; i < nPlaces; i++)
		{
			vars[i] = new LPVariable(TAG_PCOMPONENT + i, Double.class);
		}
		for(int i = 0; i < nResources; i++)
		{
			vars[nPlaces + i] = new LPVariable(TAG_RESOURCE + i, Double.class);
			//vars[nPlaces + i] = new LPVariable(TAG_RESOURCE + i, Integer.class);
		}
		return vars;
	}

	@Override
	protected LPResult processResult(Result result) {
		/* Process LP result */
		LPResult finalResult = null;
		
		double[] auxD = new double[nResources + nPlaces];
		String[] aux2 = new String[auxD.length];
		
		/* First, resources increment */
		for(int i = 0; i < nResources; i++)
		{
			aux2[i] = this.petriNet.getPlaceID(idxResources[i]);
			auxD[i] = result.get(TAG_RESOURCE + i).doubleValue();
		}
		
		/* Then, PSemiflow */
		for(int i = 0; i < nPlaces; i++)
		{
			aux2[nResources + i] = this.petriNet.getPlaceID(i);
			auxD[nResources + i] = result.get(TAG_PCOMPONENT + i).doubleValue();
		}
		
		try {
			finalResult = new LPResult(auxD, aux2);
		} catch (ResultException e) {
			e.printStackTrace();
		}
				
		return finalResult;
	}

	@Override
	protected LPVarBound[] getVarUpperBounds() {
		return null;
	}

	@Override
	protected LPVarBound[] getVarLowerBounds() {
		return null;
	}

}
