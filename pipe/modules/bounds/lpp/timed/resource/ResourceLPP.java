/**
 * Filename: ResourceLPP.java
 * Date: January, 2012 -- first release
 * 
 * Abstract class, just for clustering LP problems related to resource
 * computation of Petri nets
 *
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */
package pipe.modules.bounds.lpp.timed.resource;

import pipe.modules.bounds.lpp.timed.TimedLPP;

public abstract class ResourceLPP extends TimedLPP {


}
