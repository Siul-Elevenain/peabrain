/**
 * Filename: LPResult.java
 * Date: January, 2012 -- first release
 * Defines a class for collecting results from a LP solution
 *
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */

package pipe.modules.bounds.lpp;

import pipe.modules.bounds.errors.ResultException;

public class LPResult {
	private double[] results;
	private String[] variables;

	/**
	 * Constructor. It needs the results and the variables.
	 * There is a relationship between each component of the variables and results
	 * @param results A double vector with the numeric values 
	 * @param variables A String vector with the corresponding identification of the variable
	 * @throws ResultException An exception if dimensions of results and variables don't match
	 */
	public LPResult(double[] results, String[] variables) throws ResultException
	{
		/* Check dimensions */
		if(results.length != variables.length)
			throw new ResultException("Error processing results from LPP: dimension dismatchs.");
		
		this.results = results;
		this.variables = variables;
	}
	
	/**
	 * Returns the results vector
	 * @return A double vector containing the results
	 */
	public double[] getResult() {
		return results;
	}

	/**
	 * Returns the variables identificators
	 * @return A String vector containing the variables identificators
	 */
	public String[] getVariables() {
		return variables;
	}

	/**
	 * Method to print a LPResult as a String
	 */
	public String toString()
	{
		StringBuilder s = new StringBuilder();
		for(int i = 0; i< results.length;  i++)
		{
			s.append(variables[i] + ": " + results[i] + "\n");
		}
		return s.toString();
	}
	
	/**
	 * Checks if a variable s is contained in this result
	 * @param s Variable identification to look for
	 * @return false if s is not contained in variables, true otherwise
	 */
	public boolean isVariable(String s)
	{
		for(int i = 0; i < variables.length; i++)
			if(variables[i].equals(s) && results[i] > 0)
				return true;
		return false;
	}
}
