/**
 * Filename: Solver.java
 * Date: January, 2012 -- first release
 *
 * Abstract class for defining LP solvers. It uses JavaILP library
 *
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */
package pipe.modules.bounds.lpp.solvers;

import net.sf.javailp.Problem;
import net.sf.javailp.Result;
import net.sf.javailp.SolverFactory;

public abstract class Solver {
	private SolverFactory factory;
	/* Solver parameters */
	private int VERBOSE = 0;
	private double	TIMEOUT = 100;
		
	/**
	 * Solves the LP problem given as parameter
	 * @param problem LP problem to be solved
	 * @return Result of LP problem
	 */
	public abstract Result solve(Problem problem);

	/**
	 * Gets the SolverFactory used
	 * @return A SolverFactory object
	 */
	public SolverFactory getFactory() {
		return factory;
	}

	/**
	 * Sets a new solver. Moreover, it sets the default parameters
	 * for the LP solver: verbose off and timeout equal to TIMEOUT
	 * @param factory Solver factory to be used
	 */
	public void setFactory(SolverFactory factory) {
		this.factory = factory;
		/* Set default parameters */
		this.factory.setParameter(net.sf.javailp.Solver.VERBOSE, VERBOSE); 
		this.factory.setParameter(net.sf.javailp.Solver.TIMEOUT, TIMEOUT); // set timeout to 100 seconds
	}
	
	/**
	 * Sets the timeout to return when calling to LP solver
	 * @param timeOut New timeout to be set
	 */
	public void setTimeout(double timeOut)
	{
		this.factory.setParameter(net.sf.javailp.Solver.TIMEOUT, timeOut);
	}
}
