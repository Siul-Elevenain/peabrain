/**
 * Filename: SolverLPSolve.java
 * Date: January, 2012 -- first release
 * Defines an interface to invoke lp_solve LP solver
 *
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */
package pipe.modules.bounds.lpp.solvers;

import net.sf.javailp.Problem;
import net.sf.javailp.Result;
import net.sf.javailp.SolverFactoryLpSolve;;

public class SolverLPSolve extends Solver{

	/**
	 * Default constructor, sets lp_solve as LP solver
	 */
	public SolverLPSolve()
	{
		this.setFactory(new SolverFactoryLpSolve());
	}
	
	/**
	 * Sets lp_solve as LP solver and it changes the TIMEOUT option
	 * @param TIMEOUT to be set
	 */
	public SolverLPSolve(double TIMEOUT)
	{
		this();
		this.getFactory().setParameter(net.sf.javailp.Solver.TIMEOUT, TIMEOUT);
	}


	@Override
	public Result solve(Problem problem)
	{
		net.sf.javailp.Solver solver = this.getFactory().get(); // you should use this solver only once for one problem
		return solver.solve(problem);
	}


}
