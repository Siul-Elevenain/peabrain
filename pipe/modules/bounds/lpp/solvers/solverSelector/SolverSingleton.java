/**
 * Filename: SolverSelector.java
 * Date: January, 2014 -- first release
 * 
 * SolverSelector Singleton pattern.
 * 
 * @author (C) Iván Pamplona (University of Zaragoza, 2014) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */

package pipe.modules.bounds.lpp.solvers.solverSelector;

import java.io.FileInputStream;
import java.util.Properties;
import pipe.modules.bounds.lpp.solvers.*;

public class SolverSingleton {
	private static Solver INSTANCE = null;
	
	private static Properties dbProps = new Properties();
	private static String fileconfig = ".solverselector.conf";
	private static String userhome = System.getProperty("user.home");
	private static String solvername = null;

	private SolverSingleton(){}
	
    private synchronized static void createInstance() {
        
    	if (INSTANCE == null) 
            INSTANCE = getSolver();
    }
	
    public static Solver getInstance() {
        createInstance();
        return INSTANCE;
    }
    
    public static void setSolverCLI(String solver)
    {
		solvername = solver;
	}
    
	private static Solver getSolver() {
		String solverToUse = solvername;
		
       	try {
       		
       		if (solverToUse == null){
   				userhome = userhome.replace("\\", "/");
       		
   				String fileconfigPath = userhome + "/" + fileconfig;
   				dbProps.load(new FileInputStream(fileconfigPath));
   				solverToUse = dbProps.getProperty("Default");
       		}
	   		if (solverToUse.equals("GLPK")){
	   			return new SolverGLPK();
	   		}else if (solverToUse.equals("Lp_solve")){
	   			return new SolverLPSolve();
	   		}else if (solverToUse.equals("ILOG_CPLEX")){
	   			return new SolverGLPK(); // XXX Change this
	   		}else{
	   			throw new Exception("Undefined solver. Please check your cfg file (or configure through GUI)"); // Throw an exception if not matches
       		}
       	} catch (Exception e) {
       		e.printStackTrace();
       		System.out.println("ERROR: solver " + solverToUse + " not available. Using default solver GLPK");
   		}
       	return new SolverGLPK();
	}
	
	/**
	 * Creates a default config file
	 */
	private void createDefaultConfigFile()
	{
		
	}
}
