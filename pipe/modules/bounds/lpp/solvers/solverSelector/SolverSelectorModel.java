/**
 * Filename: SolverSelectorModel.java
 * Date: January, 2014 -- first release
 * 
 * Implements the Model for Solver Selector MVC.
 * 
 * @author (C) Ivan Pamplona (University of Zaragoza, 2014) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */

package pipe.modules.bounds.lpp.solvers.solverSelector;

import java.io.*;
import java.util.Properties;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.FileInputStream;

public class SolverSelectorModel {

	static Properties dbProps = new Properties();
	static String fileconfig = ".solverselector.conf";
	static String userhome = System.getProperty("user.home");
	static String[][] datos = null ;

	public SolverSelectorModel(){
		if (!getConfExist()) {
			setDefaultConf();
		}
	}
	
	/**
	 * Get solver list from the config file and check if default solver is installed on the system
	 * @param (no params)
	 * @return Strings Vector with list of solvers
	 */
	public String[][] getSolverList() {
		String [][] vStrings = null;
		int numTotalSolvers = this.getNumSolvers();
		vStrings = new String[numTotalSolvers][3];
		
		try {
			userhome = userhome.replace("\\", "/");
			String fileconfigPath = userhome + "/" + fileconfig;
			dbProps.load(new FileInputStream(fileconfigPath));

			for (int i=1; i <= numTotalSolvers ;i++){
    			if (!dbProps.getProperty("Solver"+i).equals("none") ){
    				
    				String [ ] palabra = dbProps.getProperty("Solver"+i).split(",");
    				vStrings[i-1][0] = palabra[0];
    				vStrings[i-1][2] = palabra[1];
    				if ( new File(palabra[1]).exists() ){ 
    					//System.out.println(dbProps.getProperty("PathSolver"+i) + " AVAILABLE");
    					vStrings[i-1][1] = "Available";
    				}else {
    					//System.out.println(dbProps.getProperty("PathSolver"+i) + " NOT available");
    					vStrings[i-1][1] = "Not Available";
    				}
    			}
			}
		} catch (Exception e) {
       		
       	}

		//Alphabetically ordered vector solvers
			String comp = null;
			String comp2 = null;
			String comp3 = null;

			for (int i = 0 ; i < vStrings.length ; i++){
				   for (int j = 1; j < vStrings.length; j ++){
					    comp = vStrings[j][0];
					    comp2 = vStrings[j][1];	   
					    comp3 = vStrings[j][2];
					    //System.out.println("\nResultado comparar: " + vStrings[j - 1][0] + " con " + comp + " = " + vStrings[j - 1][0].compareTo(comp));
				    	if (vStrings[j - 1][0].compareTo(comp) > 0){
				    		vStrings[j][0] = vStrings[j - 1][0];
				    		vStrings[j][1] = vStrings[j - 1][1];
				    		vStrings[j][2] = vStrings[j - 1][2];
				    		vStrings[j - 1][0] = comp;
				    		vStrings[j - 1][1] = comp2;
				    		vStrings[j - 1][2] = comp3;

				    	}
				    }
				}
		datos = vStrings;
		return vStrings;
	}
	
	/**
	 * Set default config if it doesn't exist.
	 * @param (no params required)
	 * @return 0 if it's OK, 1 if errors
	 */
	public int setDefaultConf() {
		
	   	try {
	   		userhome = userhome.replace("\\", "/");
	   		
	   		String fileconfigPath = userhome + "/" + fileconfig;
	   		System.out.print(fileconfigPath);

    		//set the properties value
	   		dbProps.setProperty("Default", "none");
	   		dbProps.setProperty("Solver1", "Lp_solve,/usr/lib/lp_solve");
	   		dbProps.setProperty("Solver2", "ILOG_CPLEX,/usr/lib/ILOG_CPLEX");
	   		dbProps.setProperty("Solver3", "Gurobi,/usr/lib/Gurobi");
	   		dbProps.setProperty("Solver4", "Mosek,/usr/lib/Mosek");
	   		dbProps.setProperty("Solver5", "GLPK,/usr/lib/libglpk.so");
	   		dbProps.setProperty("Solver6", "SAT4J,/usr/lib/SAT4J");
	   		dbProps.setProperty("Solver7", "MiniSat+,/usr/lib/MiniSat+");
	   		
	   		//save properties to project root folder
	   		dbProps.store(new FileOutputStream(fileconfigPath), null);
	   		return 0;

	   		
    	} catch (IOException ex) {
    		ex.printStackTrace();
       		return 1;

        }
   		
	}
	
	/**
	 * Get default solver from config file.
	 * @param (no params required)
	 * @return (String with default solver)
	 */
	public String getDefaultSolver() {
		String defaultSolver = null;
       	try {
       		userhome = userhome.replace("\\", "/");
	   		
	   		String fileconfigPath = userhome + "/" + fileconfig;
	   		dbProps.load(new FileInputStream(fileconfigPath));
	   		return dbProps.getProperty("Default");
	   		
       	} catch (Exception e) {
        }
       	return defaultSolver;
	}
	
	/**
	 * Set default solver in file config.
	 * @param (int: id solver)
	 */
	public void setDefaultSolver(int DefaultSolver) {
       	try {
    			userhome = userhome.replace("\\", "/");
    			String fileconfigPath = userhome + "/" + fileconfig;
    			dbProps.load(new FileInputStream(fileconfigPath));
    			dbProps.setProperty("Default", getStringID(DefaultSolver));
    			dbProps.store(new FileOutputStream(fileconfigPath), null);
    		} catch (Exception e) {           	}
	}
	
	/**
	 * Verify solver availavility
	 * @param (Object: value)
	 * @return boolean if solver is available.
	 */
	public boolean getSolverAvailable(Object value) {
		boolean available = false;
		
		for (int i=0; i<datos.length;i++){
			if ( datos[i][1].equals("Available") && datos[i][0].equals(value.toString()) ){ 
				available=true;
			}
		}
		return available;
	}
	
	/**
	 * Get id solver
	 * @param (String: Solver name)
	 * @return int with ID solver.
	 */
	public int getIDString(String Solver) {
   		String [][] vStrings = null;
		int numTotalSolvers = this.getNumSolvers();
		vStrings = new String[numTotalSolvers][2];
		int resultado = this.getNumSolvers();
		
			try {
			userhome = userhome.replace("\\", "/");
			String fileconfigPath = userhome + "/" + fileconfig;
			dbProps.load(new FileInputStream(fileconfigPath));

			for (int i=1; i <= numTotalSolvers ;i++){
    			if (!dbProps.getProperty("Solver"+i).equals("none") ){
    				
    				String [ ] palabra = dbProps.getProperty("Solver"+i).split(",");
    				vStrings[i-1][0] = palabra[0];
    				if ( new File(palabra[1]).exists() ){ 
    					vStrings[i-1][1] = "Available";
    				}else {
    					vStrings[i-1][1] = "Not Available";
    				}
    			}
			}
			String comp = null;
			String comp2 = null;
			for (int i = 0 ; i < vStrings.length ; i++){
				   for (int j = 1; j < vStrings.length; j ++){
					    comp = vStrings[j][0];
					    comp2 = vStrings[j][1];	   
				    	if (vStrings[j - 1][0].compareTo(comp) > 0){
				    		vStrings[j][0] = vStrings[j - 1][0];
				    		vStrings[j][1] = vStrings[j - 1][1];
				    		vStrings[j - 1][0] = comp;
				    		vStrings[j - 1][1] = comp2;
				    	}
				    }
			}
			for (int i = 0 ; i < vStrings.length ; i++){
				if (vStrings[i][0].compareTo(Solver) == 0){
					resultado = i;
				}
			}
   		} catch (Exception e) {   }
		return resultado;
	}
	
	/**
	 * Get name solver from ID
	 * @param (int: id solver)
	 * @return String: name of solver
	 */
	public String getStringID(int ID){
      	try {
       		String [][] vStrings = null;
    		int numTotalSolvers = this.getNumSolvers();
    		vStrings = new String[numTotalSolvers][2];
    		
    		
    			userhome = userhome.replace("\\", "/");
    			String fileconfigPath = userhome + "/" + fileconfig;
    			dbProps.load(new FileInputStream(fileconfigPath));

    			for (int i=1; i <= numTotalSolvers ;i++){
        			if (!dbProps.getProperty("Solver"+i).equals("none") ){
        				
        				String [ ] palabra = dbProps.getProperty("Solver"+i).split(",");
        				vStrings[i-1][0] = palabra[0];
        				if ( new File(palabra[1]).exists() ){ 
        					vStrings[i-1][1] = "Available";
        				}else {
        					vStrings[i-1][1] = "Not Available";
        				}
        			}
    			}
    			String comp = null;
    			String comp2 = null;
    			for (int i = 0 ; i < vStrings.length ; i++){
    				   for (int j = 1; j < vStrings.length; j ++){
    					    comp = vStrings[j][0];
    					    comp2 = vStrings[j][1];	   
    				    	if (vStrings[j - 1][0].compareTo(comp) > 0){
    				    		vStrings[j][0] = vStrings[j - 1][0];
    				    		vStrings[j][1] = vStrings[j - 1][1];
    				    		vStrings[j - 1][0] = comp;
    				    		vStrings[j - 1][1] = comp2;
    				    	}
    				    }
    			}
    			return vStrings[ID][0];
    		} catch (Exception e) {           }
		return null;
	}
	
	/**
	 * Get amount solvers
	 * @param (no params required)
	 * @return int num of solvers.
	 */
    public int getNumSolvers() {
       	try {
       		int numSolvers=0;
    		userhome = userhome.replace("\\", "/");
    		String fileconfigPath = userhome + "/" + fileconfig;
	   		dbProps.load(new FileInputStream(fileconfigPath));
	   		
	   		BufferedReader reader = new BufferedReader(new FileReader(fileconfigPath));
	   		int lines = 0;
	   		while (reader.readLine() != null) lines++;
	   		reader.close();
	   			   		
    		for (int i=1; i<=lines-2 ;i++){
    			if (!dbProps.getProperty("Solver"+i).equals("none") ){
    				numSolvers++;
    			}
    		}
    		return numSolvers;
       	} catch (Exception e) {
       		return 0;
       	}
    }
    
	/**
	 * Verify if file config exists and the integrity of the file
	 * @param (no params required)
	 * @return boolean: true if config exists and false if config doesn't exist.
	 */
    public boolean getConfExist(){
	   	try {
	   		userhome = userhome.replace("\\", "/");
		
	   		String fileconfigPath = userhome + "/" + fileconfig;
	   		dbProps.load(new FileInputStream(fileconfigPath));
	   		if (!dbProps.isEmpty()) {
	   				System.out.println("El archivo " + fileconfigPath + " ha sido correctamente cargado\n");
	   				System.out.println("\n" + "default = " + dbProps.getProperty("Default") +"\n" );
	   				return true;
	   		}else{ 
		   	    	System.out.println("El archivo de configuración NO ha sido correctamente cargado"); 
	   				return false;
	   		}
	   		
	   	} catch (Exception e) {
	   	    System.out.println("El archivo de configuración NO ha sido correctamente cargado"); 
	   	    return false;
	   	}
	   	
    	
    }    
}
