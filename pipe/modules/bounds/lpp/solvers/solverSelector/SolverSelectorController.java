/**
 * Filename: SolverSelectorController.java
 * Date: January, 2014 -- first release
 * 
 * Implements the Controller for Solver Selector MVC.
 * 
 * @author (C) Ivan Pamplona (University of Zaragoza, 2014) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */

package pipe.modules.bounds.lpp.solvers.solverSelector;

public class SolverSelectorController {
	
	private SolverSelectorModel model = null;
	private MyTableModel mtm = null;
	
	public SolverSelectorController(SolverSelectorModel m, MyTableModel mt) {
		this.model = m;
		this.mtm = mt;
	}
	
	public void applyButton(int DefaultConf) {
		if (mtm.getValueAt(DefaultConf, 1).toString().equals("Available")){
			model.setDefaultSolver(DefaultConf);
		}
	}
	
	public void acceptButton(String DefaultConf) {
	}
}
