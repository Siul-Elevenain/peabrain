// TODO Cabeceras, etc
package pipe.modules.bounds.lpp.structural;

import net.sf.javailp.Linear;
import net.sf.javailp.OptType;
import net.sf.javailp.Result;
import pipe.modules.bounds.errors.ResultException;
import pipe.modules.bounds.lpp.LPConstants;
import pipe.modules.bounds.lpp.LPConstraint;
import pipe.modules.bounds.lpp.LPObjectiveFunc;
import pipe.modules.bounds.lpp.LPResult;
import pipe.modules.bounds.lpp.LPVarBound;
import pipe.modules.bounds.lpp.LPVariable;

public class StructuralImplicitPlace extends StructuralLPP {
	public static final String TAG_PCOMPONENT = "y";
	
	private final int idxPlace;
	
	public StructuralImplicitPlace(int idxPlace)
	{
		this.idxPlace = idxPlace;
	}
	
	public StructuralImplicitPlace()
	{
		this(0);
		System.out.println("Please choose properly the place to compute if it is a Structurally Implicit Place. Set p0 by default.");
	}
	@Override
	protected int checkOtherErrors() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected LPConstraint[] loadConstraints() {
		LPConstraint[] constraints = new LPConstraint[nPlaces + nTransitions + 1];
		
		/* Create inequality constraints */
		// y >= 0
		for(int i = 0; i < nPlaces; i++)
		{
			Linear linear = new Linear();
			linear.add(1, TAG_PCOMPONENT + i);
			
			constraints[i] = new LPConstraint(linear, LPConstants.GREATEST_THAN_OR_EQUAL, 0);
		}
		// y'·C <= C[p]
		int[][] C = this.petriNet.getIncidenceMatrix();
		for(int j = 0; j < nTransitions; j++)
		{
			Linear auxLinear = new Linear();
			for(int i = 0; i < nPlaces; i++)
			{
				if(C[i][j] != 0)
				{
					auxLinear.add(C[i][j], TAG_PCOMPONENT + i);
				}
			}
			constraints[nPlaces + j] = new LPConstraint(auxLinear, LPConstants.LOWEST_THAN_OR_EQUAL, C[this.idxPlace][j]);
		}
		
		// y[p] = 0
		Linear linear = new Linear();
		linear.add(1, TAG_PCOMPONENT + idxPlace);
		constraints[nPlaces + nTransitions] = new LPConstraint(linear, LPConstants.EQUAL, 0);
		
		return constraints;
	}

	@Override
	protected LPObjectiveFunc loadObjectiveFunction() {
		LPObjectiveFunc objFunc;
		
		Linear linear = new Linear();
		// y*m0
		for(int i = 0; i < this.nPlaces; i++)
			linear.add(this.petriNet.getInitialMarkingAtPlace(i), TAG_PCOMPONENT + i);
		
		/* Set objective function*/
		objFunc = new LPObjectiveFunc(linear, OptType.MIN);
		
		return objFunc;
	}

	@Override
	protected LPVariable[] loadVariablesType() {
		LPVariable[] vars = new LPVariable[nPlaces];
		
		for(int i = 0; i < nPlaces; i++)
		{
			vars[i] = new LPVariable(TAG_PCOMPONENT + i, Double.class);
		}
		
		return vars;
	}

	@Override
	protected LPResult processResult(Result result) {
		LPResult finalResult = null;
		
		double[] aux = new double[1];
		String[] aux2 = new String[1];
		// If you reach this, ALWAYS THERE EXISTS A SOLUTION (i.e., result != null)
		aux[0] = 1; // 1 -> TRUE
		aux2[0] = this.petriNet.getPlaceID(idxPlace);
		
		try
		{
			finalResult = new LPResult(aux, aux2);
		} catch (ResultException e) {
			e.printStackTrace();
		}
		
		return finalResult;
	}

	@Override
	protected LPVarBound[] getVarUpperBounds() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected LPVarBound[] getVarLowerBounds() {
		// TODO Auto-generated method stub
		return null;
	}

}
