/**
 * Filename: VisitRatio.java
 * Date: January, 2012 -- first release
 * 
 * Computes the visit ratio v, normalised for transition i,
 * for a PN model.
 * 
 *  ( C )
 *  ( R )· v_i = 0
 *  
 *  v_i(i) = 1
 *  
 *  where C is the incidence matrix of the PN, and R the rates matrix
 *
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */
package pipe.modules.bounds.lpp.structural;


import java.util.ArrayList;

import Jama.Matrix;

import net.sf.javailp.Linear;
import net.sf.javailp.OptType;
import net.sf.javailp.Result;
import pipe.modules.bounds.dataLayer.PetriNetModel;
import pipe.modules.bounds.errors.ResultException;
import pipe.modules.bounds.lpp.LPConstants;
import pipe.modules.bounds.lpp.LPConstraint;
import pipe.modules.bounds.lpp.LPObjectiveFunc;
import pipe.modules.bounds.lpp.LPResult;
import pipe.modules.bounds.lpp.LPVarBound;
import pipe.modules.bounds.lpp.LPVariable;

public class VisitRatio  extends StructuralLPP
{
	private final String TAG_VISITRATIO = "v";
	private int idxNormalisedTransition;
	private double[][] matrix;

	/**
	 * Default constructor. It needs the Petri net model for compute its
	 * visti ratio, and the index of transition to be used as normalised transition
	 * @param petriNet Petri net model
	 * @param idxNormalisedTransition Index of transition
	 */
	
	public VisitRatio(PetriNetModel petriNet, int idxNormalisedTransition)
	{
		super();
		super.setPetriNet(petriNet);
		this.idxNormalisedTransition = idxNormalisedTransition;
	}
	
	@Override
	public int checkOtherErrors()
	{
		/* XXX: Visit ratio can be computed in any PN
		 * when just depends on its structure and not the 
		 * initial marking (i.e., avoid simple nets ¿?) 
		 */
		
		return 0;
	}

	@Override
	protected LPConstraint[] loadConstraints() {
		/*
		 * ( C )
		 * ( R ) · v_j = 0
		 * 
		 * v_j(j) = 1
		 * */
		
		// Compute number of transitions in conflict, and stores it
		ArrayList<ArrayList<Integer>>  conflictPlaces = new ArrayList<ArrayList<Integer>>(1);
		int nConstraints = 0;
		for(int i = 0; i < nPlaces; i++)
		{
			ArrayList<Integer>  v = this.petriNet.findInRowPre(i);
			
			if(v.size() > 1)
			{
				boolean freeConflict = true;
				for(int j = 0; j < v.size() && freeConflict; j++)
				{
					// We need to check now if it's a free-conflict
					ArrayList<Integer> c = this.petriNet.findInColPre(v.get(j));
					freeConflict &= c.size() == 1;
				}
				if(freeConflict)
				{
					conflictPlaces.add(v);
					// Compute new constraints
					int newConstraints = v.size() - 1;
					// Compute sum of new constraints needed, as n*(n + 1)/2
					nConstraints += newConstraints*(newConstraints + 1)/2;
				}
			}
			//XXX: We assume only immediate transitions are in conflict...
		}
		
		LPConstraint[] constraints = new LPConstraint[nPlaces + nConstraints + 1];
		
		matrix = new double[constraints.length][nTransitions];
		
		for(int i = 0; i < nPlaces; i++)
		{
			Linear auxLinear = new Linear();
			for(int j = 0; j < nTransitions; j++)
			{
				int aux = this.petriNet.getIncidenceMatrix()[i][j];
				if(aux != 0)
				{	
					auxLinear.add(aux, TAG_VISITRATIO + j);
					matrix[i][j] = aux;
				}
			}
			constraints[i] = 
					new LPConstraint(auxLinear, LPConstants.EQUAL, 0);
		}
		
		int idx = nPlaces;
		for(int i = 0; i < conflictPlaces.size(); i++)
		{
			ArrayList<Integer>  v = (ArrayList<Integer>)conflictPlaces.get(i); /* Get conflict transitions */
			
			for(int j = 0; j < v.size(); j++)
			{
				int firstElmnt = (int)v.get(j);
				double value = -this.petriNet.getTransitionWeight(firstElmnt);
			
				for(int k = (j  + 1); k < v.size(); k++)
				{
					Linear auxLinear = new Linear();
					int aux = (int)v.get(k);
					
					double aux2 = this.petriNet.getTransitionWeight(aux);
					auxLinear.add(value,
								TAG_VISITRATIO + aux);
				
					matrix[idx][aux] = value;
					
					auxLinear.add(aux2, 
							TAG_VISITRATIO + firstElmnt);
					matrix[idx][firstElmnt] = aux2;

					constraints[idx] = 
						new LPConstraint(auxLinear, LPConstants.EQUAL, 0);
					idx++;
				}
			}
			
		}
		
		/* Last constraint: normalised transition */
		Linear auxLinear = new Linear();
		auxLinear.add(1, TAG_VISITRATIO + this.idxNormalisedTransition);
		constraints[nConstraints + nPlaces] = new LPConstraint(auxLinear, LPConstants.EQUAL, 1);
	
		return constraints;
	}

	@Override
	protected LPObjectiveFunc loadObjectiveFunction() {
		LPObjectiveFunc objFunc;
		
		Linear linear = new Linear();
		//for(int i = 0; i < nTransitions; i++)
		//	linear.add(1,  TAG_VISITRATIO + i);
		linear.add(1,  TAG_VISITRATIO + 0);
		
		/* Set objective function*/
		objFunc = new LPObjectiveFunc(linear, OptType.MAX);
		
		return objFunc;
	}

	@Override
	protected LPVariable[] loadVariablesType() {
		LPVariable[] vars = new LPVariable[nTransitions];
		
		for(int i = 0; i < nTransitions; i++)
		{
			vars[i] = new LPVariable(TAG_VISITRATIO + i, Double.class);
		}
		
		return vars;
	}

	@Override
	protected LPResult processResult(Result result) {
		LPResult finalResult = null;
		double[] aux = new double[nTransitions];
		String[] aux2 = new String[aux.length];
		
		for(int i = 0; i < nTransitions; i++)
		{
			aux2[i] = TAG_VISITRATIO + i;
			aux[i] = result.get(TAG_VISITRATIO + i).doubleValue();
		}
				
		try {
			finalResult = new LPResult(aux, aux2);
		} catch (ResultException e) {
			e.printStackTrace();
		}
		
		return finalResult;
	}

	@Override
	protected LPVarBound[] getVarUpperBounds() {
		return null;
	}

	@Override
	protected LPVarBound[] getVarLowerBounds() {
		return null;
	}

	/**
	 * Checks if the matrix (C R)' has a unique solution
	 * If so, then we know that visit ratio can be computed
	 * @return A boolean indicating if the visit ratio matrices has a unique solution or not
	 */
	public boolean hasUniqueSolution()
	{
		if(matrix == null)
		{
			loadConstraints();
		}
		
		/* Check the range of the matrix */
		Matrix m = new Matrix(matrix);
		int rank = m.rank();
		
		return (rank == (this.petriNet.getTransitionsSize() - 1));
	}
}
