/**
 * Filename: HValue.java
 * Date: January, 2012 -- first release
 * Computes the h value useful for computing the next slowest p-semiflow
 * 
 * max h
 *    s.t. y'·C = 0
 *         y'·m0 = 1
 *         y >= h·1
 *         h > 0
 *  where y is a p-semiflow which covers all places.
 *         
 * TODO: Add reference to paper if accepted 
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */
package pipe.modules.bounds.lpp.structural;

import net.sf.javailp.Linear;
import net.sf.javailp.OptType;
import net.sf.javailp.Result;
import pipe.modules.bounds.errors.ResultException;
import pipe.modules.bounds.lpp.LPConstants;
import pipe.modules.bounds.lpp.LPConstraint;
import pipe.modules.bounds.lpp.LPObjectiveFunc;
import pipe.modules.bounds.lpp.LPResult;
import pipe.modules.bounds.lpp.LPVarBound;
import pipe.modules.bounds.lpp.LPVariable;

public class HValue extends StructuralLPP {
	public final String TAG_PCOMPONENT = "y",
						TAG_H = "h";
	
	@Override
	protected int checkOtherErrors() {
		// No constraints at least for now
		return 0;
	}

	@Override
	protected LPConstraint[] loadConstraints() {
		LPConstraint[] constraints = new LPConstraint[nPlaces + 2 + nTransitions];
		
		/* Create inequality constraints */
		for(int i = 0; i < nPlaces; i++)
		{
			Linear linear = new Linear();
			linear.add(1, TAG_PCOMPONENT + i);
			linear.add(-1, "h");
			
			constraints[i] = new LPConstraint(linear, LPConstants.GREATEST_THAN_OR_EQUAL, 0);
		}
		Linear auxLinear = new Linear();
		auxLinear.add(1, TAG_H);
		constraints[nPlaces] = new LPConstraint(auxLinear, LPConstants.GREATEST_THAN_OR_EQUAL, 0);
		
		/* Create equality constraints */
		/* Y'·m0 = 1*/
		auxLinear = new Linear();
		for(int i = 0; i < nPlaces; i++)
			if(this.petriNet.isInitialMarked(i))
				auxLinear.add(this.petriNet.getInitialMarkingAtPlace(i), TAG_PCOMPONENT + i);
		
		constraints[nPlaces + 1] = new LPConstraint(auxLinear, LPConstants.EQUAL, 1);
		
		/* Y'·C = 0 */
		int[][] C = this.petriNet.getIncidenceMatrix();
		for(int j = 0; j < nTransitions; j++)
		{
			auxLinear = new Linear();
			for(int i = 0; i < nPlaces; i++)
			{
				if(C[i][j] != 0)
				{
					auxLinear.add(C[i][j], TAG_PCOMPONENT + i);
				}
			}
			constraints[(nPlaces + 2) + j] = new LPConstraint(auxLinear, LPConstants.EQUAL, 0);
		}
		
		return constraints;
	}

	@Override
	protected LPObjectiveFunc loadObjectiveFunction() {
		LPObjectiveFunc objFunc;
		
		Linear linear = new Linear();
		linear.add(1,  TAG_H);
		
		/* Set objective function*/
		objFunc = new LPObjectiveFunc(linear, OptType.MAX);
		
		return objFunc;
	}

	@Override
	protected LPVariable[] loadVariablesType() {
		LPVariable[] vars = new LPVariable[nPlaces + 1];
		
		// Y components
		for(int i = 0; i < nPlaces; i++)
		{
			vars[i] = new LPVariable(TAG_PCOMPONENT + i, Double.class);
		}
		// h
		vars[nPlaces] = new LPVariable(TAG_H, Double.class);
		return vars;
	}

	@Override
	public LPResult processResult(Result result) {
		/* Process LP result */
		LPResult finalResult = null;
		
		double[] aux = new double[1];
		String[] aux2 = new String[1];

		aux[0] = result.get(TAG_H).doubleValue();
		aux2[0] = TAG_H;
		
		try {
			finalResult = new LPResult(aux, aux2);
		} catch (ResultException e) {
			e.printStackTrace();
		}
		
		return finalResult;
	}

	@Override
	protected LPVarBound[] getVarUpperBounds() {
		/* No bounds needed */
		return null;
	}

	@Override
	protected LPVarBound[] getVarLowerBounds() {
		/* No bounds needed */
		return null;
	}

}
