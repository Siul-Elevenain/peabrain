// TODO
// Acabar de rellenar + cabecera...
package pipe.modules.bounds.lpp.structural;

import java.util.ArrayList;

import net.sf.javailp.Linear;
import net.sf.javailp.OptType;
import net.sf.javailp.Result;
import pipe.modules.bounds.errors.ResultException;
import pipe.modules.bounds.lpp.LPConstants;
import pipe.modules.bounds.lpp.LPConstraint;
import pipe.modules.bounds.lpp.LPObjectiveFunc;
import pipe.modules.bounds.lpp.LPResult;
import pipe.modules.bounds.lpp.LPVarBound;
import pipe.modules.bounds.lpp.LPVariable;

public class InitialMarkingSIP extends StructuralLPP {
	public static final String TAG_PCOMPONENT = "y";
	private final String TAG_MU = "u";
	
	private final int idxPlace;
	
	public InitialMarkingSIP(int idxPlace)
	{
		this.idxPlace = idxPlace;
	}
	
	public InitialMarkingSIP()
	{
		this(0);
		System.out.println("Please choose properly the place to compute its initial marking to make p implicit (being a Structurally Implicit Place). Set p0 by default.");
	}
	@Override
	protected int checkOtherErrors() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected LPConstraint[] loadConstraints() {
		ArrayList<Integer> v = this.petriNet.findInRowPre(idxPlace);
		LPConstraint[] constraints = new LPConstraint[nPlaces + nTransitions + v.size() + 1];
		
		/* Create inequality constraints */
		// y >= 0
		for(int i = 0; i < nPlaces; i++)
		{
			Linear linear = new Linear();
			linear.add(1, TAG_PCOMPONENT + i);
			
			constraints[i] = new LPConstraint(linear, LPConstants.GREATEST_THAN_OR_EQUAL, 0);
		}
		// y'·C <= C[p, T]
		int[][] C = this.petriNet.getIncidenceMatrix();
		for(int j = 0; j < nTransitions; j++)
		{
			Linear auxLinear = new Linear();
			for(int i = 0; i < nPlaces; i++)
			{
				if(C[i][j] != 0)
				{
					auxLinear.add(C[i][j], TAG_PCOMPONENT + i);
				}
			}
			constraints[nPlaces + j] = new LPConstraint(auxLinear, LPConstants.LOWEST_THAN_OR_EQUAL, C[this.idxPlace][j]);
		}
		
		// y·PRE[P, t] + \mu >= Pre[p, t], \forall t \in \postset{p}
		int idx = nPlaces + nTransitions;
		int[][] Pre = this.petriNet.getPre();
		for(int j = 0; j < v.size(); j++)
		{
			int auxIdx = v.get(j);
			Linear auxLinear = new Linear();
			for(int i = 0; i < nPlaces; i++)
				if(Pre[i][auxIdx] != 0)
					auxLinear.add(Pre[i][auxIdx], TAG_PCOMPONENT + i);
			auxLinear.add(1, TAG_MU);
			
			constraints[idx + j] = new LPConstraint(auxLinear, LPConstants.GREATEST_THAN_OR_EQUAL, Pre[this.idxPlace][auxIdx]);
		}
		idx += v.size();
		// y[p] = 0
		Linear linear = new Linear();
		linear.add(1, TAG_PCOMPONENT + idxPlace);
		constraints[idx] = new LPConstraint(linear, LPConstants.EQUAL, 0);
		
		return constraints;
	}

	@Override
	protected LPObjectiveFunc loadObjectiveFunction() {
		LPObjectiveFunc objFunc;
		
		Linear linear = new Linear();
		// y*m0
		for(int i = 0; i < this.nPlaces; i++)
			linear.add(this.petriNet.getInitialMarkingAtPlace(i), TAG_PCOMPONENT + i);
		// alpha = 1, alpha · \mu
		linear.add(1, this.TAG_MU);
		
		/* Set objective function*/
		objFunc = new LPObjectiveFunc(linear, OptType.MIN);
		
		return objFunc;
	}

	@Override
	protected LPVariable[] loadVariablesType() {
		LPVariable[] vars = new LPVariable[nPlaces + 1];
		
		for(int i = 0; i < nPlaces; i++)
		{
			vars[i] = new LPVariable(TAG_PCOMPONENT + i, Double.class);
		}
		vars[nPlaces] = new LPVariable(TAG_MU, Double.class);

		return vars;
	}

	@Override
	protected LPResult processResult(Result result) {
		LPResult finalResult = null;
		
		double[] aux = new double[1];
		String[] aux2 = new String[1];
		// max(0, z), where z = y*m0 + \mu
		aux[0] = result.get(TAG_MU).doubleValue();
		for(int i = 0; i < nPlaces; i++)
			if(this.petriNet.isInitialMarked(i))
				aux[0] += result.get(TAG_PCOMPONENT + i).doubleValue()*this.petriNet.getInitialMarkingAtPlace(i);
		
		
		aux2[0] = this.petriNet.getPlaceID(idxPlace);
		
		try
		{
			finalResult = new LPResult(aux, aux2);
		} catch (ResultException e) {
			e.printStackTrace();
		}
		
		return finalResult;
	}

	@Override
	protected LPVarBound[] getVarUpperBounds() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected LPVarBound[] getVarLowerBounds() {
		// TODO Auto-generated method stub
		return null;
	}

}
