/**
 * Filename: StructuralMarking.java
 * Date: January, 2012 -- first release
 * Computes the structural marking bound of a place p
 * 
 * max m(p)
 *    s.t. 	m = m0 + C·\sigma
 *    		m, \sigma >= 0
 *    
 *    where m0 is the initial marking, and \sigma the firing count vector
 *
 * See J. Campos and M. Silva, 
 * "Embedded Product-Form Queueing Networks and the Improvement of Performance Bounds for Petri Net Systems," 
 * Performance Evaluation, vol. 18, iss. 1, pp. 3-19, 1993.
 * http://webdiis.unizar.es/CRPetri/papers/jcampos/93_CS_PE.ps.gz
 * 
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */
package pipe.modules.bounds.lpp.structural;

import net.sf.javailp.Linear;
import net.sf.javailp.OptType;
import net.sf.javailp.Result;
import pipe.modules.bounds.errors.ResultException;
import pipe.modules.bounds.lpp.LPConstants;
import pipe.modules.bounds.lpp.LPConstraint;
import pipe.modules.bounds.lpp.LPObjectiveFunc;
import pipe.modules.bounds.lpp.LPResult;
import pipe.modules.bounds.lpp.LPVarBound;
import pipe.modules.bounds.lpp.LPVariable;

public class StructuralMarking extends StructuralLPP {
	public static final String TAG_PLACE = "p";
	private final String TAG_SIGMA = "s";
	
	private final int idxPlace;
	
	/**
	 * Constructor. It needs the index of the place for computing
	 * its structural marking
	 * @param idxPlace Index of place to compute its structural marking
	 */
	public StructuralMarking(int idxPlace)
	{
		this.idxPlace = idxPlace;
	}
	
	/**
	 * Default Constructor. Assumes transition t0 set, if no other is chosen.
	 */
	public StructuralMarking()
	{
		this(0);
		System.out.println("Please choose properly the place to compute its structural marking. Set p0 by default.");
	}
	
	@Override
	protected LPConstraint[] loadConstraints() {
		LPConstraint[] constraints = new LPConstraint[nPlaces + nTransitions + nPlaces];
		
		/* Create inequality constraints */
		for(int i = 0; i < nPlaces; i++)
		{
			Linear linear = new Linear();
			linear.add(1, TAG_PLACE + i);
			
			constraints[i] = new LPConstraint(linear, LPConstants.GREATEST_THAN_OR_EQUAL, 0);
		}
		for(int i = 0; i < nTransitions; i++)
		{
			Linear linear = new Linear();
			linear.add(1, TAG_SIGMA + i);
			
			constraints[nPlaces + i] = new LPConstraint(linear, LPConstants.GREATEST_THAN_OR_EQUAL, 0);
		}
		
		/* Create equality constraints */
		
		int aux;
		for(int i = 0; i < nPlaces; i++)
		{
			Linear auxLinear = new Linear();
			auxLinear.add(1, TAG_PLACE + i); // m(p_i)
			for(int j = 0; j < nTransitions; j++)
			{
				aux = -this.petriNet.getIncidenceMatrix()[i][j];
				if(aux != 0)
					auxLinear.add(aux, TAG_SIGMA + j);
			}
			// Create constraint m_p - C(p,:)·\sigma = m0(p)
			constraints[(i + nPlaces + nTransitions)] = 
					new LPConstraint(auxLinear, LPConstants.EQUAL, this.petriNet.getInitialMarkingAtPlace(i));
		}
		
		return constraints;
	}

	
	@Override
	protected LPObjectiveFunc loadObjectiveFunction() {
		LPObjectiveFunc objFunc;
		
		Linear linear = new Linear();
		linear.add(1, TAG_PLACE + idxPlace);
		
		/* Set objective function*/
		objFunc = new LPObjectiveFunc(linear, OptType.MAX);
		
		return objFunc;
	}

	@Override
	protected LPVariable[] loadVariablesType() {
		LPVariable[] vars = new LPVariable[nPlaces + nTransitions];
		
		for(int i = 0; i < nPlaces; i++)
		{
			vars[i] = new LPVariable(TAG_PLACE + i, Double.class);
		}
		for(int i = 0; i < nTransitions; i++)
		{
			vars[nPlaces + i] = new LPVariable(TAG_SIGMA + i, Double.class);
		}

		return vars;
	}

	@Override
	protected LPResult processResult(Result result) {
		/* Process LP result */
		LPResult finalResult = null;
		double[] aux = new double[1];
		String[] aux2 = new String[1];
		aux[0] = result.get(TAG_PLACE + idxPlace).doubleValue();
		aux2[0] = this.petriNet.getPlaceID(idxPlace);
				
		try {
			finalResult = new LPResult(aux, aux2);
		} catch (ResultException e) {
			e.printStackTrace();
		}
		
		return finalResult;
	}

	@Override
	protected int checkOtherErrors() {
		/* No other additional constraints needed to check */
		return 0;
	}

	@Override
	protected LPVarBound[] getVarUpperBounds() {
		/* No bounds needed */
		return null;
	}

	@Override
	protected LPVarBound[] getVarLowerBounds() {
		/* No bounds needed */
		return null;
	}

}
