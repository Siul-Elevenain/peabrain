/**
 * Filename: StructuralBoundGUI.java
 * Date: January, 2012 -- first release
 * Creates a GUI for collecting information needed (epsilon) for computing
 * the structural marking at a place and structural enabling at a transition
 * for a Petri net
 * 
 * See StructuralIterator.java
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */
package pipe.modules.bounds.gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;

import javax.swing.ButtonGroup;

import pipe.modules.bounds.strategies.StructuralIterator;
import pipe.modules.bounds.strategies.StructuralType;

import javax.swing.JRadioButton;
import javax.swing.JComboBox;

import java.awt.event.ActionListener;

public class StructuralBoundGUI extends PeabraiNGUI {
	private static final long serialVersionUID = 6425587045634542905L;

	private JComboBox comboBox;
	private JRadioButton rdbtnAllItems, rdbtnSelectItem;
	private StructuralType sType;

	private String itemLabel, titleText; 
	
	private String[] getItemsData() {
		/* Create a string for each place/transition */
		
		try{
			String[] vStrings = null;
			
			switch(sType)
			{
				case STRUCTURAL_MARKING:
				case STRUCTURAL_IMPLICIT_PLACE:
					vStrings = new String[this.pnModel.getPlacesSize()];
					for(int i = 0; i < vStrings.length; i++)
						vStrings[i] = this.pnModel.getPlaceID(i);
					
					break;
				case STRUCTURAL_ENABLING:
					vStrings = new String[this.pnModel.getTransitionsSize()];
					for(int i = 0; i < vStrings.length; i++)
						vStrings[i] = this.pnModel.getTransitionID(i);
					break;
			}
	
			return vStrings;
		}
		catch(Exception e)
		{
			return null;
		}
	}

	private void generateLabels()
	{
		switch(sType)
		{
			case STRUCTURAL_MARKING:
				itemLabel = "place";
				titleText = "Structural Marking";
				break;
			case STRUCTURAL_IMPLICIT_PLACE:
				itemLabel = "place";
				titleText = "Structural Implicit Place";
				break;
			case STRUCTURAL_ENABLING:
				itemLabel = "transition";
				titleText = "Structural Enabling";
				break;
		}
	}
	
	public StructuralBoundGUI(StructuralIterator si)
	{
		super(si.getCurrentPNModel(), false, si);
		this.sType = si.getStructuralTypeSelected();
		createAdditionals();
		this.setTitle(getGUIName());
	}

	protected void enableComputeButton() {
		if(!(pnModel.getPlacesSize() == 0 || pnModel.getTransitionsSize() == 0))
			this.btnCompute.setEnabled(rdbtnAllItems.isSelected() || rdbtnSelectItem.isSelected());
	}

	protected void compute()
	{
		/* Disable button */
		btnCompute.setEnabled(false);
		
		/* Get selected place */
		/* Two choices: all places, or one place... */
		if(this.rdbtnAllItems.isSelected())
		{
			/* All places selected */
			/* Compute the structural marking for all places, and show results */
			
			((StructuralIterator)strategy).computeAllItems();
		}
		else
		{
			/* Get the selected place */
			int idxPlace = this.comboBox.getSelectedIndex();
			((StructuralIterator)strategy).computeOneItem(idxPlace);
		}
		
		/* Get and show results */
		setText(strategy.printResult());
	}
	


	@Override
	protected void createAdditionalGUIElements() {
		try{
			generateLabels();
		}
		catch(Exception e)
		{
			// Avoid error
			sType = StructuralType.STRUCTURAL_ENABLING;
		}
		
		pnlData.setPreferredSize(new Dimension(250, 75));
		pnlData.setLayout(null);
		
		rdbtnAllItems = new JRadioButton("All " + itemLabel + "s");
		rdbtnAllItems.setBounds(8, 8, 149, 23);
		rdbtnAllItems.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				enableComputeButton();
			}
		});
		pnlData.add(rdbtnAllItems);
				
		rdbtnSelectItem = new JRadioButton("Select " + itemLabel + ":");
		rdbtnSelectItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				enableComputeButton();
			}
		});
		rdbtnSelectItem.setBounds(8, 37, 150, 23);
		pnlData.add(rdbtnSelectItem);
		
		/* Group buttons */
		ButtonGroup group = new ButtonGroup();
		group.add(rdbtnAllItems);
		group.add(rdbtnSelectItem);
		
		comboBox = new JComboBox(getItemsData());
		//comboBox = new JComboBox<String>();
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				rdbtnSelectItem.setSelected(true);
				enableComputeButton();
			}
		});
		comboBox.setBounds(160, 36, 80, 24);
		pnlData.add(comboBox);
		
	}

	@Override
	protected String getGUIName() {
		return titleText + " Computation";
	}
}
