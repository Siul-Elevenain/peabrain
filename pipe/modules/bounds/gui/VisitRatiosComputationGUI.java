/**
 * Filename: VisitRatiosComputationGUI.java
 * Date: January, 2012 -- first release
 * Creates a GUI for collecting information needed for computing
 * visit ratio of a Petri net
 *  
 * See VisitRatiosStrategy.java
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>. 
 */
package pipe.modules.bounds.gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import pipe.modules.bounds.strategies.VisitRatiosStrategy;

import javax.swing.JLabel;

public class VisitRatiosComputationGUI extends PeabraiNGUI {

	private static final long serialVersionUID = 1L;
	private JComboBox cmbBoxNormalisedTransition;
	
	
	private String[] getItemsData() {
		/* Create a string for each place/transition */
		
		String[] vStrings = new String[this.pnModel.getTransitionsSize()];
		
		for(int i = 0; i < vStrings.length; i++)
			vStrings[i] = this.pnModel.getTransitionID(i);

		return vStrings;
	}

	public VisitRatiosComputationGUI(VisitRatiosStrategy vrs)
	{
		super(vrs.getCurrentPNModel(), true, vrs);
	}

	protected void enableComputeButton() {
		this.btnCompute.setEnabled(
				(!(pnModel.getPlacesSize() == 0 || pnModel.getTransitionsSize() == 0)));
	}

	protected void compute()
	{
		/* Disable button */
		btnCompute.setEnabled(false);

		try{
			/* Set transitions */
			((VisitRatiosStrategy)this.strategy).
				setNormalisedTransition((String)this.cmbBoxNormalisedTransition.getSelectedItem());
			/* Get visit ratio */
			strategy.compute();		
			/* Get and show results */
			setText(strategy.printResult());
		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	

	@Override
	protected void createAdditionalGUIElements() {
		pnlData.setPreferredSize(new Dimension(300, 50));
		pnlData.setLayout(null);

		cmbBoxNormalisedTransition = new JComboBox(getItemsData());// getItemsData()
		cmbBoxNormalisedTransition.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				enableComputeButton();
			}
		});
		cmbBoxNormalisedTransition.setBounds(208, 18, 80, 24);
		pnlData.add(cmbBoxNormalisedTransition);
		
		
		JLabel lblComputeVisitRatio = new JLabel("Compute visit ratio");
		lblComputeVisitRatio.setBounds(14, 12, 255, 15);
		pnlData.add(lblComputeVisitRatio);
		
		JLabel label = new JLabel("normalised for transition:");
		label.setBounds(14, 23, 255, 15);
		pnlData.add(label);
	}

	@Override
	protected String getGUIName() {
		return "Visit Ratios Computation";
	}

}
