/**
 * Filename: MyParamValue.java
 * Date: January, 2014 -- first release
 * 
 * Param Class for parameters of differents PeaBrain Modules.
 *
 * @author (C) Iván Pamplona (University of Zaragoza, 2014) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>. 
 */

package pipe.modules.bounds.cli;

public class MyParamValue {
	private String paramChar; 	//parameter (Example: -f)
	private String desc;  		//short description 
	private String descfull; 	//full description
	private boolean optional;	//optional parameter: true/false
	private Object value; 		//Value 
	private boolean assigned;	//assigned parameter: true/false
	private int params;			//1 = single param., 2 = value required

	public MyParamValue(String paramChar, String desc, String descfull, Boolean optional, Object value, boolean assigned, int params) {
		this.paramChar=paramChar; 
		this.desc=desc;
		this.descfull=descfull;
		this.optional=optional;
		this.value=value;
		this.assigned=assigned;
		this.params=params;
	}
	
	public void setValue(Object o){
		if (this.value.getClass().getSimpleName().equals("Double")){
			try {
				this.value= Double.parseDouble(o.toString());
			}catch(NumberFormatException exception){
				System.out.println("\nERROR: Wrong Double parameter type");
				System.exit(1);
			}
		}else if (this.value.getClass().getSimpleName().equals("String")){
			try {
				this.value= o.toString();
			} catch (Exception e) {
				//e.printStackTrace();
				System.out.println("\nERROR: Wrong String parameter type");
				System.exit(1);
			}
		}else if (this.value.getClass().getSimpleName().equals("Integer")){
			try {
				this.value= Integer.parseInt(o.toString());
			} catch (Exception e) {
				//e.printStackTrace();
				System.out.println("\nERROR: Wrong Integer parameter type");
				System.exit(1);
			}
		}else{
			System.out.println("Wrong parameters");
			System.exit(1);
		}
	}
	
	public Object getValue(){
		return this.value;
	}
	
	public String getParamChar(){
		return this.paramChar;
	}
	
	public String getDesc(){
		return this.desc;
	}
	
	public String getDescFull(){
		return this.descfull;
	}
	
	public boolean getAssigned(){
		return this.assigned;
	}
	
	public void setAssigned(boolean a){
		this.assigned=a;
	}
	
	public boolean getOptional(){
		return this.optional;
	}
	
	public int getParams(){
		return this.params;
	}
	
	public void setOptional(boolean a){
		this.optional=a;
	}
	
	
}
