/**
 * Filename: PerformanceEstimationCLI.java
 * Date: January, 2014 -- first release
 * 
 * Abstract class to instance modules of Peabrain.
 * 
 * See PerformanceEstimationStrategy.java
 * @author (C) Ivan Pamplona (University of Zaragoza, 2014) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */


package pipe.modules.bounds.cli;

import java.io.File;

import pipe.modules.bounds.dataLayer.PetriNetModel;
import pipe.modules.bounds.lpp.solvers.solverSelector.SolverSingleton;
import pipe.modules.bounds.strategies.Strategy;
import org.jsoup.*;
import pipe.views.PetriNetView;

import java.io.StringWriter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


public abstract class PeabraiNCLI {
	
	protected MyParamValue[] myParamValue;
	protected PetriNetModel pnModel;
	protected Strategy strategy;
	private String xmlFilename;
	protected String callcli;
	protected int lastparamlenght;
	private boolean checkAndFilled = false;
	
	public PeabraiNCLI() {
		buildParamValueVector();
	}
	
	public void checkAndFillParams(String[] args){
	
		myParamValue[0] = new MyParamValue("-f","-file","-file",false,new String(),false,2);
		myParamValue[1] = new MyParamValue("-solver","-solver","-solver",true,new String(),false,2);
		myParamValue[2] = new MyParamValue("-fancy-output","html format output","html format ouput",true,new String(),false,1);
		
		//Check number of params
		int max_params=2;
		int min_params=2;
		for (int i = 0; i < this.getParamValueLenght(); i++){
			if (!myParamValue[i].getOptional()){
				max_params=max_params + myParamValue[i].getParams();
				min_params=min_params + myParamValue[i].getParams();
			}else{
				max_params=max_params + myParamValue[i].getParams();
			}
		}
		if (args.length < min_params || args.length > max_params){
			System.out.println("ERROR: Wrong number params");
			System.exit(1);
		}
		
		//from the 3th param until last, check args
		for (int i=2; i <= (args.length)-1; i=i+lastparamlenght){
			int start=-1;
			//Check if param exists
			for (int j=0; j < this.getParamValueLenght(); j++){
				if ( args[i].equals(this.myParamValue[j].getParamChar()) )
					if (this.myParamValue[j].getAssigned()){
						System.out.println("ERROR: Duplicate argument");
						System.exit(1);
					}else
						start=j;
			}
			
			//if args is not found, exit
			if (start == -1) {System.out.println("\nERROR: wrong parameter " +  args[i]); System.exit(1);}
			
			//Check if param need value
			if (this.myParamValue[start].getParams() == 2) myParamValue[start].setValue(args[i+1]);
			
			//Mark param as assigned
			myParamValue[start].setAssigned(true);
			
			//lastparamlenght is used for the "for" count
			lastparamlenght = this.myParamValue[start].getParams() ;
		}
		
		//Check if all no optional values are assigned
		for (int i=0; i<this.getParamValueLenght(); i++){
			if (!this.myParamValue[i].getAssigned() && !this.myParamValue[i].getOptional()){
				System.out.println("Need more parameters");
				System.exit(1);
			}
		}
		checkAndFilled = true;
	}

	
	private void loadXMLFile()
	{
			//PetriNetView pnview = new PetriNetView(xmlFilename, false);
			pnModel = new PetriNetModel(new PetriNetView(xmlFilename));
	}
	
	public String getFile(){
		return xmlFilename;
	}
	
	public String html2text (String html){
		return Jsoup.parse(html).text();
	}
	
	public int getParamValueLenght(){
		return this.myParamValue.length;
	}
	
	public void getModuleHelp(String module)
	{
		System.out.println("Module name: " + module);
		System.out.println("=========================");
		System.out.println("Module Parameters:");
		System.out.println(" -m <module name> ");
		System.out.println(" -f <PetriNetFile.xml> ");
		System.out.println(" -solver <Solver> ");
		System.out.println(" -fancy-output //Dump output in html format ");

		for (int i=3; i<myParamValue.length; i++){
			System.out.println(" " + myParamValue[i].getParamChar() + " <" + myParamValue[i].getDesc() + ">" + " // " + myParamValue[i].getDescFull() ); 
		}
		
		System.out.println("=========================");
		genXML(module);
		System.exit(0);
	}
	
	public void genXML(String module){
		 try {
	            //String[] input = {"John Doe,123-456-7890", "Bob Smith,123-555-1212"};
	            //String[] line = new String[2];
	            String bool = null;
	            
	            DocumentBuilderFactory dFact = DocumentBuilderFactory.newInstance();
	            DocumentBuilder build = dFact.newDocumentBuilder();
	            Document doc = build.newDocument();
	            
	            Element root = doc.createElement(module);
	            doc.appendChild(root);
	            
	            Element memberList = doc.createElement("parameters");
	            root.appendChild(memberList);
	            
	            for (int i=3; i<myParamValue.length; i++){
	                Element var = doc.createElement("var");
	                memberList.appendChild(var);
	                Element param = doc.createElement("param");
	                param.appendChild(doc.createTextNode(myParamValue[i].getParamChar()));
	                var.appendChild(param);
	                
	                Element type = doc.createElement("type");
	                //Check if parameter need value or not
	                if (myParamValue[i].getValue()==null) type.appendChild(doc.createTextNode("null"));
	                else type.appendChild(doc.createTextNode(myParamValue[i].getValue().getClass().getSimpleName()));
	                var.appendChild(type);
	                
	                Element description = doc.createElement("description");
	                description.appendChild(doc.createTextNode(myParamValue[i].getDesc()));
	                var.appendChild(description);
	                
	                Element optional = doc.createElement("optional");
	                if (myParamValue[i].getOptional() ) bool="true"; else bool="false";
	                optional.appendChild(doc.createTextNode( bool ));
	                var.appendChild(optional);
	            }
	            
	            TransformerFactory tFact = TransformerFactory.newInstance();
	            Transformer trans = tFact.newTransformer();

	            StreamResult result = new StreamResult(new File("/tmp/" + module + ".xml"));

	            StringWriter writer = new StringWriter();
	            DOMSource source = new DOMSource(doc);
	            trans.transform(source, result);
	            System.out.println(writer.toString());

	        } catch (TransformerException ex) {
	            System.out.println("Error outputting document");
	        } catch (ParserConfigurationException ex) {
	            System.out.println("Error building document");
	        }
	}
	
	public void compute()
	{	
		if(!checkAndFilled)
		{
			System.out.println("Please check your params!");
			return;
		}
		
		xmlFilename = (String) this.myParamValue[0].getValue();
	
		//Check if filename exists
		File f = new File(xmlFilename);
		if(!f.exists()) {
			System.out.println("File doesn't exist " + xmlFilename);
			System.exit(1);
		}
		// Load it
		loadXMLFile();
		
		// Check if some solver has been selected
		String solvSelected = myParamValue[1].getValue().toString();
		if(solvSelected.length() != 0) // XXX Other way to check this?
			SolverSingleton.setSolverCLI(solvSelected);
	
		createStrategy();
		strategy.compute();
	}
	
	public void getResults()
	{
		callcli = (String) this.myParamValue[2].getValue();
		String text = strategy.printResult().toString();

		if (!this.myParamValue[2].getAssigned()){
			text = text.replaceAll("&plusmn;","+");
			text = text.replaceAll("<br/>","\n");
			text = text.replaceAll("</td>","\t|\t");
			text = text.replaceAll("</tr>","\n");
			text = text.replaceAll("\\<table.*?>","\n");
			text = text.replaceAll("\\<.*?>","");
			System.out.println("=========================");
			System.out.println(text);
			System.out.println("=========================");
		}else{		
			//HTML FORMAT
			System.out.println(text);
		}
	}
	
	public abstract void buildParamValueVector();
	protected abstract void createStrategy();
	
	

}
