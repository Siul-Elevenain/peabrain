/**
 * Filename: ResultException.java
 * Date: January, 2012 -- first release
 * Auxiliary class for having our 'own' exceptions :)
 *  
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */
package pipe.modules.bounds.errors;

public class ResultException extends Exception {
	private static final long serialVersionUID = 4013760279000900868L;

	public ResultException(String exceptionMsg) {
		super(exceptionMsg);
	}

}
