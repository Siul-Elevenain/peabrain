/**
 * Filename: PipeCLI.java
 * Date: January, 2014 -- first release
 * 
 * Receive parameters from CLI to Peabrain.
 *
 * @author (C) Iván Pamplona (University of Zaragoza, 2014) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>. 
 */

import pipe.gui.ApplicationSettings;
import pipe.gui.Export;
import pipe.gui.PetriNetTab;
import pipe.modules.bounds.cli.PeabraiNCLI;
import pipe.views.PipeApplicationView;
import pipe.models.PipeApplicationModel;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class PipeCli {

	public static void main(String args[]) throws Exception {

		PipeCli pipecli = new PipeCli();
		pipecli.processArgs(args);
	}

	private void processArgs(String[] args) {

		Class module = null;
		Constructor emptyConstructor = null;
		PeabraiNCLI myModule = null;

		if (args[0].equals("-topng")) {

			if (args.length < 3) {
				System.out.println("\nERROR: wrong parameters");
				System.out.println("\nargs: -topng <PetriNet.xml> <output_file.png>\n");
				System.exit(1);
			}

			PipeApplicationView pipeApplicationView = ApplicationSettings.getApplicationView();

			File f = new File(args[1].toString());

			pipeApplicationView.createNewTab(f, false);
			PetriNetTab appView = pipeApplicationView.getCurrentTab();
			try {
				appView.updatePreferredSize();
				Export.toPNG(appView, args[2].toString());
				System.exit(0);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (args.length < 3) {
			help_base();
		} else if (args[0].equals("-m")) {

			module = check_module(args[1]);
			if (module != null) {
				try {
					emptyConstructor = module.getConstructor();
					myModule = (PeabraiNCLI) emptyConstructor.newInstance();
				} catch (Exception e) {
					// e.printStackTrace();
					System.out.println(e);
				}

				if (args[2].equals("-h") || args[2].equals("--help")) {
					myModule.getModuleHelp(args[1]);
				} else {
					myModule.checkAndFillParams(args);
					myModule.compute();
					myModule.getResults();
				}
			} else
				help_base();
		} else
			help_base();
	}

	private Class check_module(String module) {
		System.out.println("\nChecking module...");
		String clase;
		try {
			clase = "pipe.modules.bounds.cli.modules." + module;
			return Class.forName(clase);
		} catch (ClassNotFoundException e) {
			System.out.println("Module " + module + " doesn't exist");
			return null;
		}
	}

	private void help_base() {

		String str = null;
		String[] temp = null;
		String delimiter = "\\.";

		try {
			Class[] classes = getClasses("pipe.modules.bounds.cli.modules");
			System.out.println("\nUse: PipeCLI -m <module> -h\n");
			System.out.println("------ AVAILABLE MODULES ------");

			for (Class c : classes) {
				str = c.toString();
				temp = str.split(delimiter);
				System.out.println("- " + temp[temp.length - 1]);
			}
			System.out.println("\n");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Scans all classes accessible from the context class loader which belong
	 * to the given package and subpackages.
	 * 
	 * @param packageName
	 *            The base package
	 * @return The classes
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */

	private Class[] getClasses(String packageName)
			throws ClassNotFoundException, IOException {
		ClassLoader classLoader = Thread.currentThread()
				.getContextClassLoader();
		assert classLoader != null;
		String path = packageName.replace('.', '/');
		Enumeration<URL> resources = classLoader.getResources(path);
		List<File> dirs = new ArrayList<File>();
		while (resources.hasMoreElements()) {
			URL resource = resources.nextElement();
			dirs.add(new File(resource.getFile()));
		}
		ArrayList<Class> classes = new ArrayList<Class>();
		for (File directory : dirs) {
			classes.addAll(findClasses(directory, packageName));
		}
		return classes.toArray(new Class[classes.size()]);
	}

	/**
	 * Recursive method used to find all classes in a given directory and
	 * subdirs.
	 * 
	 * @param directory
	 *            The base directory
	 * @param packageName
	 *            The package name for classes found inside the base directory
	 * @return The classes
	 * @throws ClassNotFoundException
	 */
	private List<Class> findClasses(File directory, String packageName)
			throws ClassNotFoundException {
		List<Class> classes = new ArrayList<Class>();
		if (!directory.exists()) {
			return classes;
		}
		File[] files = directory.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				assert !file.getName().contains(".");
				classes.addAll(findClasses(file,
						packageName + "." + file.getName()));
			} else if (file.getName().endsWith(".class")) {
				classes.add(Class.forName(packageName
						+ '.'
						+ file.getName().substring(0,
								file.getName().length() - 6)));
			}
		}
		return classes;
	}
}